<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientDetail extends Model
{
    protected $fillable = [


        'patient_name', 'card_no','cr_no','health_card_scheme','ward','address','mobile','package_name','package_code','approved_amount','whether_package_changed','previous_package_detail',
        'medco_name','date','case_no','admit_date','discharged_date','whether_package_extend','extended_approved_amount','status'



    ];
}
