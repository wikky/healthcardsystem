<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use Auth;
use App;
use Hash;
use File;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class AdminController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 9.1  List All Healthcards For Admin  ********************************


    public function get_health_card_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
              
        $health_card = \App\HealthCard::where('id','<>',0);

        $health_card = $health_card->orderBy($order_by,$order);

        $result = $health_card->paginate($per_page)->appends(request()->query());

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Health Card List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 9.2  List All Tests For Admin  ********************************


    public function get_lab_test_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
        $department = $this->validate_var(@$_GET['department'],'');
        $test_id = $this->validate_var(@$_GET['test_id'],'');
              
        $lab_tests = \App\LabTest::where('status',1);

        if($department != '' && $department != null)
        {
            
            $lab_tests = $lab_tests->where('department',$department);
        }

        if($test_id != '' && $test_id != null)
        {
            
            $lab_tests = $lab_tests->where('id',$test_id);
        }


        $lab_tests = $lab_tests->orderBy($order_by,$order);

        $result = $lab_tests->paginate($per_page)->appends(request()->query());

        foreach($result as $newresult)
        {
            $newresult->department_detail = \App\LabDepartment::where('id',$newresult->department)->get();
        }

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Lab Test List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 9.3  List All Lab Departments for Admin ********************************


    public function get_lab_department_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
              
        $lab_departments = \App\LabDepartment::where('id','<>',0);

        $lab_departments = $lab_departments->orderBy($order_by,$order);

        $result = $lab_departments->paginate($per_page)->appends(request()->query());

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Lab Department List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }



    //*************************** Route No. 9.4  List All User Type for Admin ********************************


    public function get_user_type_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
              
        $user_type = \App\UserType::where('id','<>',0);

        $user_type = $user_type->orderBy($order_by,$order);

        $result = $user_type->paginate($per_page)->appends(request()->query());

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'User Type List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }
}