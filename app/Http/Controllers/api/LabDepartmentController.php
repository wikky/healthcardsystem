<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use Auth;
use App;
use Hash;
use File;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class LabDepartmentController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 3.1  Generate Lab Department  ********************************


    public function generate_lab_department(Request $request)
    {
        
            // *********** Check for required fields ****************


            $validator=Validator::make($request->all(), [

                'title'             => 'required',
            ]);

            if($validator->errors()->all())
            {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = $validator->errors()->first();
                return $data;    
            }


            $title = $this->validate_var(@$request->title, '');
            $identifier = strtolower(str_replace(' ', '_', $title));

            $lab_department_status = \App\LabDepartment::where('title',$title)->where('identifier',$identifier)->count();

            if($lab_department_status > 0)
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Lab Department Already Exist';
                return $data;
            }


            // *********** Store data into lab_departments table ****************

            $lab_department = new App\LabDepartment;
            $lab_department->title = $title;
            $lab_department->identifier = $identifier;
            $lab_department->save();

            $lab_department_id = $lab_department['id'];


            $lab_department_data = @\App\LabDepartment::where('id',$lab_department_id)->get();


            if($lab_department_data != '')
            {
                $data['status_code']    =  1;
                $data['status_text']    =  'Success';
                $data['message']        =  'Lab Department Generated Successfully';
                $data['data']           =  $lab_department_data;
            }
            else
            {
                $data['status_code']    =  0;
                $data['status_text']    =  'Failed';
                $data['message']        =  'Unable To Generate Lab Department';
                $data['data']           =  [];
            } 
        
        return $data;                 
    }



    //*************************** Route No. 3.2  List All Lab Departments  ********************************


    public function get_lab_department_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
              
        $lab_departments = \App\LabDepartment::where('status',1);

        $lab_departments = $lab_departments->orderBy($order_by,$order);

        $result = $lab_departments->paginate($per_page)->appends(request()->query());

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Lab Department List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 3.3  Update Lab Department  ********************************

    
    public function update_lab_department(Request $request,$id)
    {
          
        $title = $this->validate_var(@$request->title, '');
        $identifier = strtolower(str_replace(' ', '_', $title));

        $health_card = \App\LabDepartment::where('id',$id)->update([


            'title' => $title,
            'identifier' => $identifier


        ]);


        $result = \App\LabDepartment::where('id',$id)->get();

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Lab Department Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Lab Department';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 3.4  Delete Lab Department ********************************


    public function destroy($id)
    {
                
        @\App\LabDepartment::where('id',$id)->update([

            'status' => 2

        ]);
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';             
        $data['message']        =   'Lab Department Deleted Successfully';
                                                
        return $data;
    }


    //*************************** Route No. 3.5  Update status of Lab Department ********************************


    public function update_lab_department_status(Request $request,$id)
    {
        
        $status = $this->validate_var(@$request->status,'');
        
        $lab_department = \App\LabDepartment::where('id',$id)->update([


            'status' => $status


        ]);

        $result = \App\LabDepartment::where('id',$id)->get();


        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Status Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Status';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }

}