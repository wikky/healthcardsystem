<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use Auth;
use App;
use Hash;
use File;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class LabTestController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 4.1  Generate Lab Test  ********************************


    public function generate_lab_test(Request $request)
    {
        
            // *********** Check for required fields ****************


            $validator=Validator::make($request->all(), [

                'department'        => 'required',
                'title'             => 'required',
                'amount'            => 'required',

            ]);

            if($validator->errors()->all())
            {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = $validator->errors()->first();
                return $data;    
            }


            $department = $this->validate_var(@$request->department, '');
            $amount = $this->validate_var(@$request->amount, '');
            $title = $this->validate_var(@$request->title, '');
            $identifier = strtolower(str_replace(' ', '_', $title));

            $lab_department_status = \App\LabTest::where('department',$department)->where('title',$title)->where('identifier',$identifier)->count();

            if($lab_department_status > 0)
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Lab Test Already Exist';
                return $data;
            }


            // *********** Store data into lab_tests table ****************

            $lab_test = new App\LabTest;
            $lab_test->department = $department;
            $lab_test->title = $title;
            $lab_test->identifier = $identifier;
            $lab_test->amount = $amount;
            $lab_test->save();

            $lab_test_id = $lab_test['id'];


            $lab_test_data = @\App\LabTest::where('id',$lab_test_id)->get();


            if($lab_test_data != '')
            {
                $data['status_code']    =  1;
                $data['status_text']    =  'Success';
                $data['message']        =  'Lab Test Generated Successfully';
                $data['data']           =  $lab_test_data;
            }
            else
            {
                $data['status_code']    =  0;
                $data['status_text']    =  'Failed';
                $data['message']        =  'Unable To Generate Lab Test';
                $data['data']           =  [];
            } 
        
        return $data;                 
    }



    //*************************** Route No. 4.2  List All Lab Tests  ********************************


    public function get_lab_test_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
        $department = $this->validate_var(@$_GET['department'],'');
        $test_id = $this->validate_var(@$_GET['test_id'],'');
              
        $lab_tests = \App\LabTest::where('status',1);

        if($department != '' && $department != null)
        {
            
            $lab_tests = $lab_tests->where('department',$department);
        }

        if($test_id != '' && $test_id != null)
        {
            
            $lab_tests = $lab_tests->where('id',$test_id);
        }


        $lab_tests = $lab_tests->orderBy($order_by,$order);

        $result = $lab_tests->paginate($per_page)->appends(request()->query());

        foreach($result as $newresult)
        {
            $newresult->department_detail = \App\LabDepartment::where('id',$newresult->department)->get();
        }

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Lab Test List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 4.3  Update Lab Test  ********************************

    
    public function update_lab_test(Request $request,$id)
    {
          
        $input=array_filter($request->all());

        $title = $input['title'];

        $identifier = strtolower(str_replace(' ', '_', $title));

        $input['identifier'] = $identifier;

        $lab_test = \App\LabTest::where('id',$id)->update($input);

        $result = \App\LabTest::where('id',$id)->get();

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Lab Test Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Lab Test';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 4.4  Delete Lab Test ********************************


    public function destroy($id)
    {
                
        @\App\LabTest::where('id',$id)->update([

            'status' => 2

        ]);
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';             
        $data['message']        =   'Lab Test Deleted Successfully';
                                                
        return $data;
    }



    //*************************** Route No. 4.5  Update status of Lab Test ********************************


    public function update_lab_test_status(Request $request,$id)
    {
        
        $status = $this->validate_var(@$request->status,'');
        
        $lab_test = \App\LabTest::where('id',$id)->update([


            'status' => $status


        ]);

        $result = \App\LabTest::where('id',$id)->get();


        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Status Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Status';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }

}