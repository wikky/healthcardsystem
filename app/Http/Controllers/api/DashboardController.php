<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use App;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;



class DashboardController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 8.1  Get Dashboard Data ********************************


    public function dashboard_data()
    {
        $total_users = \App\User::where('id','<>',0)->count();
        $total_patients = \App\PatientDetail::where('id','<>',0)->count();
        $total_health_cards = \App\HealthCard::where('id','<>',0)->count();

        $data['status_code']            =   1;
        $data['status_text']            =   'Success';             
        $data['message']                =   'Data Fetched Successfully';
        $data['total_users']            =    $total_users;
        $data['total_patients']         =    $total_patients;
        $data['total_health_cards']     =    $total_health_cards;
        return $data;
    }
}

       