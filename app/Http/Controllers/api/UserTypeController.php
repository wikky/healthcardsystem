<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use Auth;
use App;
use Hash;
use File;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class UserTypeController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 7.1  Generate User Type ********************************


    public function generate_user_type(Request $request)
    {
        
            // *********** Check for required fields ****************


            $validator=Validator::make($request->all(), [

                'title'             => 'required',
            ]);

            if($validator->errors()->all())
            {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = $validator->errors()->first();
                return $data;    
            }


            $title = $this->validate_var(@$request->title, '');
            $identifier = strtolower(str_replace(' ', '_', $title));

            $user_type_status = \App\UserType::where('title',$title)->where('identifier',$identifier)->count();

            if($user_type_status > 0)
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'User Type Already Exist';
                return $data;
            }


            // *********** Store data into user_types table ****************

            $user_type = new App\UserType;
            $user_type->title = $title;
            $user_type->identifier = $identifier;
            $user_type->save();

            $user_type_id = $user_type['id'];


            $user_type_data = @\App\UserType::where('id',$user_type_id)->get();


            if($user_type_data != '')
            {
                $data['status_code']    =  1;
                $data['status_text']    =  'Success';
                $data['message']        =  'User Type Generated Successfully';
                $data['data']           =  $user_type_data;
            }
            else
            {
                $data['status_code']    =  0;
                $data['status_text']    =  'Failed';
                $data['message']        =  'Unable To Generate User Type';
                $data['data']           =  [];
            } 
        
        return $data;                 
    }



    //*************************** Route No. 7.2  List All User Type  ********************************


    public function get_user_type_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
              
        $user_type = \App\UserType::where('status',1);

        $user_type = $user_type->orderBy($order_by,$order);

        $result = $user_type->paginate($per_page)->appends(request()->query());

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'User Type List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 7.3  Update User Type  ********************************

    
    public function update_user_type(Request $request,$id)
    {
          
        $title = $this->validate_var(@$request->title, '');
        $identifier = strtolower(str_replace(' ', '_', $title));

        $user_type = \App\UserType::where('id',$id)->update([


            'title' => $title,
            'identifier' => $identifier


        ]);


        $result = \App\UserType::where('id',$id)->get();

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'User Type Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update User Type';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 7.4  Delete User Type ********************************


    public function destroy($id)
    {
                
        @\App\UserType::where('id',$id)->update([

            'status' => 2

        ]);
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';             
        $data['message']        =   'User Deleted Successfully';
                                                
        return $data;
    }


    //*************************** Route No. 7.5  Update status of User Type ********************************


    public function update_user_type_status(Request $request,$id)
    {
        
        $status = $this->validate_var(@$request->status,'');
        
        $user_type = \App\UserType::where('id',$id)->update([


            'status' => $status


        ]);

        $result = \App\UserType::where('id',$id)->get();


        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Status Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Status';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }

}