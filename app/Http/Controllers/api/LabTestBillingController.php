<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use Auth;
use App;
use Hash;
use File;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class LabTestBillingController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 5.1  Generate Lab Test Bill  ********************************


    public function generate_lab_test_memo(Request $request)
    {
        
        // *********** Check for required fields ****************


        $validator=Validator::make($request->all(), [

            'cr_no'             => 'required',
            'department'        => 'required',
            'test'              => 'required',
            'amount'            => 'required',
        ]);

        if($validator->errors()->all())
        {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = $validator->errors()->first();
            return $data;    
        }


        $cr_no = $this->validate_var(@$request->cr_no,'');
        $date = $this->validate_var(@$request->date,'');
        $bill_no = $this->validate_var(@$request->bill_no,'');
        $req_no = $this->validate_var(@$request->req_no,'');

        $department = $request['department'];
        $test = $request['test'];
        $amount = $request['amount'];

        $cr_no_status = \App\LabTestDetail::where('patient_cr_no',$cr_no)->count();

        if($cr_no_status > 0)
        {
            $tag = \App\LabTestDetail::where('patient_cr_no',$cr_no)->orderBy('created_at','DESC')->first()->tag;
            $tag = $tag + 1;
        }
        else
        {
            $tag = 1;
        }

        for($i=0;$i<sizeof($department);$i++)
        {
            $department_status = \App\LabTestDetail::where('department',$department[$i])->count();

            if($department_status > 0)
            {
                $receipt_no = \App\LabTestDetail::where('department',$department[$i])->orderBy('created_at','DESC')->first()->receipt_no;

                if($receipt_no != '')
                {
                    $receipt_no = $receipt_no + 1;
                }
            }          
            else
            {
                $receipt_no = 1;
            }
        }

        
        // *********** Store data into lab_test_details table ****************

        for($i=0;$i<sizeof($department);$i++)
        {
            $test_detail = new App\LabTestDetail;
            $test_detail->date = $date;
            $test_detail->department = $department[$i];
            $test_detail->test = $test[$i];
            $test_detail->receipt_no = $receipt_no;
            $test_detail->amount = $amount[$i];
            $test_detail->memo_no = $bill_no;
            $test_detail->req_no = $req_no;
            $test_detail->tag = $tag;
            $test_detail->patient_cr_no = $cr_no;
            $test_detail->save();
        }

        $data['status_code']    =  1;
        $data['status_text']    =  'Success';
        $data['message']        =  'Lab Test Memo Generated Successfully';
        return $data;                 
    }



    //*************************** Route No. 5.2  Get Last Bill No  ********************************


    public function get_last_bill_no()
    {
        
        $status = \App\LabTestDetail::count();

        if($status > 0)
        {
            $memo_no = \App\LabTestDetail::orderBy('created_at','DESC')->first()->memo_no;
        }
        else
        {
            $memo_no = 0;
        }

        $data['status_code']    =   1;
        $data['status_text']    =   'Success';             
        $data['message']        =   'Last Bill No Fetched Successfully';
        $data['last_bill_no']      =   $memo_no;  
        return $data;
    }



    //*************************** Route No. 5.3  Get Last Requistion Number  ********************************


    public function get_last_requisition_no()
    {
        
        $status = \App\LabTestDetail::count();

        if($status > 0)
        {
            $requisition_no = \App\LabTestDetail::orderBy('req_no','DESC')->first()->req_no;
        }
        else
        {
            $requisition_no = 0;
        }

        $data['status_code']    =   1;
        $data['status_text']    =   'Success';             
        $data['message']        =   'Last Requisition No Fetched Successfully';
        $data['last_requisition_no']      =   $requisition_no;  
        return $data;
    }


    //*************************** Route No. 5.4  Get Requistion Detail  ********************************


    public function get_requisition_detail($id)
    {
        
        $result = \App\LabTestDetail::where('req_no',$id)->get(['id','date','test','department','amount','req_no','memo_no','patient_cr_no']);

        $patient_detail = \App\PatientDetail::where('id',$result[0]['patient_cr_no'])->get(['id','cr_no','patient_name','age','sex','address','mobile']);
        
        foreach($result as $newresult)
        {
            
            $newresult->department = \App\LabDepartment::where('id',$newresult->department)->first()->title;
            $newresult->test = \App\LabTest::where('id',$newresult->test)->first()->title;

        }

        if($result != '')
        {
            $data['status_code']    =  1;
            $data['status_text']    =  'Success';
            $data['message']        =  'Requisition Detail Fetched Successfully';
            $data['patient_detail']           =  $patient_detail;
            $data['data']           =  $result;
        }
        else
        {
            $data['status_code']    =  0;
            $data['status_text']    =  'Failed';
            $data['message']        =  'Unable To Fatch Requisition Detail';
            $data['data']           =  [];
        } 
    
        return $data;     
    }


    //*************************** Route No. 5.5  List All Bills for Days  ********************************


    public function get_test_bill_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'ASC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
       
                     
        $bills = \App\LabTestDetail::where('id','<>',0);

        if(isset($_GET['date_from']) && $_GET['date_from'] != '' && isset($_GET['date_to']) && $_GET['date_to'] != '' )
        {
            date_default_timezone_set('Asia/Kolkata');

            $date_from= $_GET['date_from'];
            $date_to = $_GET['date_to'];
            $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
            $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
            $bills = $bills->where('date','<=',$date_to)->where('date','>=',$date_from);
        }

        if(isset($_GET['date_from']) && $_GET['date_from'] != '' && empty($_GET['date_to']) && $_GET['date_to'] == '' )
        {
            date_default_timezone_set('Asia/Kolkata');

            $date_from= $_GET['date_from'];
            $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');

            $bills = $bills->where('date','>=',$date_from);
        }

        $bills = $bills->orderBy($order_by,$order);

        $result = $bills->paginate($per_page)->appends(request()->query());

        foreach($result as $newresult)
        {
            $newresult->department_name = \App\LabDepartment::where('id',$newresult->department)->first()->title;
            $newresult->test_name = \App\LabTest::where('id',$newresult->test)->first()->title;
            $newresult->formated_date = \Carbon\Carbon::parse($newresult->created_at,'UTC')->setTimezone('Asia/Kolkata')->format('d F, Y');
            $newresult->formated_time = \Carbon\Carbon::parse($newresult->created_at,'UTC')->setTimezone('Asia/Kolkata')->format('h:i A');
        }


        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Teat Bill List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 5.6  List All Bills of Perticular Date  ********************************


    public function get_test_bill_date($date)
    {
        
        date_default_timezone_set('Asia/Kolkata');

        $main_array = array();

        $patient_array = array();

        $cr_numbers = \App\LabTestDetail::where('date',$date)->distinct()->pluck('patient_cr_no');

        for($i=0;$i<sizeof($cr_numbers);$i++)
        {
            
            $patient_array['cr_no'] = $cr_numbers[$i];

            $tags = \App\LabTestDetail::where('patient_cr_no',$cr_numbers[$i])->where('date',$date)->distinct()->pluck('tag');

            $tags_array = array();

            for($j=0;$j<sizeof($tags);$j++)
            {
                $tags_data = \App\LabTestDetail::where('patient_cr_no',$cr_numbers[$i])->where('date',$date)->where('tag',$tags[$j])->get();

                foreach($tags_data as $new_tags_data)
                {
                    
                    $new_tags_data->department_name = \App\LabDepartment::where('id',$new_tags_data->department)->first()->title;
                    $new_tags_data->test_name = \App\LabTest::where('id',$new_tags_data->test)->first()->title;
                    $new_tags_data->formated_date = \Carbon\Carbon::parse($new_tags_data->created_at,'UTC')->setTimezone('Asia/Kolkata')->format('d F, Y');
                    $new_tags_data->formated_time = \Carbon\Carbon::parse($new_tags_data->created_at,'UTC')->setTimezone('Asia/Kolkata')->format('h:i A');
                }

                $tags_array[] = $tags_data;
            }

            $patient_array['data'] = $tags_array;

            $main_array[] = $patient_array;
        }

        

       if(sizeof($main_array) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Test Bill List Fetched Successfully';
            $data['data']      =   $main_array;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


}