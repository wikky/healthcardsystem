<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use Auth;
use App;
use Hash;
use File;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class HealthCardSchemeController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 2.1  Generate Health Card Scheme  ********************************


    public function generate_healthcard(Request $request)
    {
        
            // *********** Check for required fields ****************


            $validator=Validator::make($request->all(), [

                'title'             => 'required',
            ]);

            if($validator->errors()->all())
            {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = $validator->errors()->first();
                return $data;    
            }


            $title = $this->validate_var(@$request->title, '');
            $identifier = strtolower(str_replace(' ', '_', $title));

            $health_card_status = \App\HealthCard::where('title',$title)->where('identifier',$identifier)->count();

            if($health_card_status > 0)
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Health Card Scheme Already Exist';
                return $data;
            }


            // *********** Store data into health_cards table ****************

            $health_card = new App\HealthCard;
            $health_card->title = $title;
            $health_card->identifier = $identifier;
            $health_card->save();

            $health_card_data = $health_card['id'];


            $health_card_data = @\App\HealthCard::where('id',$health_card_data)->get();


            if($health_card_data != '')
            {
                $data['status_code']    =  1;
                $data['status_text']    =  'Success';
                $data['message']        =  'Health Card Generated Successfully';
                $data['data']           =  $health_card_data;
            }
            else
            {
                $data['status_code']    =  0;
                $data['status_text']    =  'Failed';
                $data['message']        =  'Unable To Generate Health Card';
                $data['data']           =  [];
            } 
        
        return $data;                 
    }




    //*************************** Route No. 2.2  List All Healthcards  ********************************


    public function get_health_card_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
              
        $health_card = \App\HealthCard::where('status',1);

        $health_card = $health_card->orderBy($order_by,$order);

        $result = $health_card->paginate($per_page)->appends(request()->query());

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Health Card List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 2.3  Update Health Card  ********************************

    
    public function update_health_card(Request $request,$id)
    {
          
        $title = $this->validate_var(@$request->title, '');
        $identifier = strtolower(str_replace(' ', '_', $title));

        $health_card = \App\HealthCard::where('id',$id)->update([


            'title' => $title,
            'identifier' => $identifier


        ]);


        $result = \App\HealthCard::where('id',$id)->get();

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Health Card Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Health Card';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 2.4  Delete Health Card ********************************


    public function destroy($id)
    {
                
        @\App\HealthCard::where('id',$id)->update([

            'status' => 2

        ]);
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';             
        $data['message']        =   'Health Card Deleted Successfully';
                                                
        return $data;
    }



    //*************************** Route No. 2.5  Update status of Health Card ********************************


    public function update_health_card_status(Request $request,$id)
    {
        
        $status = $this->validate_var(@$request->status,'');
        
        $health_card = \App\HealthCard::where('id',$id)->update([


            'status' => $status


        ]);

        $result = \App\HealthCard::where('id',$id)->get();


        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Status Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Status';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }
}