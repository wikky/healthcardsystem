<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use Auth;
use App;
use Hash;
use File;
use DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 6.1  Register User  ********************************


    public function register(Request $request)
	{
		// *********** Check for required fields ****************


            $validator=Validator::make($request->all(), [

                'user_type'         =>  'required',
                'first_name'        =>  'required',
                'mobile'            =>  'required',
                'email'             =>  'required',
                'password'          =>  'required',
            ]);

            if($validator->errors()->all())
            {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = $validator->errors()->first();
                return $data;    
            }


        $user_type = $this->validate_var(@$request->user_type, '');

                      
               
            // *********** Check existance of email ****************

        
            $email = $this->validate_var(@$request->email, '');
            $status = \App\User::where('email',$email)->count();
            if($status > 0)
            {
            	$data['status_code']    =  0;
                $data['status_text']    =  'Failed';
                $data['message']        =  'User Already Registered on This Email';
                $data['data']           =  [];
                return $data;
            }



        // *********** Store data into users table ****************


            $user = new App\User;
            $user->user_type = $user_type;
            $user->first_name = $this->validate_var(@$request->first_name, '');
            $user->last_name = $this->validate_var(@$request->last_name, '');
            $user->email = $this->validate_var(@$request->email, '');
            $user->password = Hash::make($this->validate_var(@$request->password, ''));
            $user->mobile = $this->validate_var(@$request->mobile, '');
            $user->landline_no = $this->validate_var(@$request->landline_no, '');
            $user->address = $this->validate_var(@$request->address, '');
            $user->save();

            $user_id = $user['id'];


            $user_data = @\App\User::where('id',$user_id)->get();


            if($user_data != '')
            {
                $data['status_code']    =  1;
                $data['status_text']    =  'Success';
                $data['message']        =  'User Registered Successfully';
                $data['data']           =  $user_data;
            }
            else
            {
                $data['status_code']    =  0;
                $data['status_text']    =  'Failed';
                $data['message']        =  'Unable To Register User';
                $data['data']           =  [];
            } 
                   
        return $data;                 
    }



        //*************************** Route No. 6.2  Login User  ********************************


    public function login(Request $request)
    {
    	
    	// *********** Check for required fields ****************

            $validator=Validator::make($request->all(), [

               	'email' 	=>  'required',
                'password' 	=>  'required',
            ]);

            if($validator->errors()->all())
            {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = $validator->errors()->first();
                return $data;    
            }

            $email 		= 	$this->validate_var(@$request->email, '');
            $password 	= 	$this->validate_var(@$request->password, '');
            $hashed_password = Hash::make($this->validate_var(@$request->password, '')); 

            $email_status = \App\User::where('email',$email)->count();

            if($email_status > 0)
            {
                $user_status = \App\User::where('email',$email)->first()->status;

                if($user_status == 0)
                {
                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message']     = 'Account Inactive';
                    return $data;
                }
            }

                       
            if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])) 
            {
                $user_id = Auth::id();

                $user_data = \App\User::where('id',$user_id)->get();

                $data['status_code'] = 1;
                $data['status_text'] = 'Success';
                $data['message']	 = 'You are login successfully';
                $data['data']		 =	$user_data;
            }
        	else
        	{
             	$data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message']	 = 'Wrong Credentials';
          	}    
        	return $data;
	}

    
    
    //*************************** Route No. 6.3  List All Users  ********************************


    public function get_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
        $user_id = $this->validate_var(@$_GET['user_id'],'');
        $user_type = $this->validate_var(@$_GET['user_type'],'');

        $users = \App\User::where('id','<>',0);


        if($user_type != '' && $user_type != null)
        {
            
            $users = $users->where('user_type',$user_type);
        }

        
        if($user_id != '' && $user_id != null)
        {
            
            $users = $users->where('id',$user_id);
        }

        $users = $users->orderBy($order_by,$order);

        $result = $users->paginate($per_page)->appends(request()->query());

        foreach($result as $newresult)
        {
            
            $user_type_title = \App\UserType::where('identifier',$newresult->user_type)->first()->title;
            $newresult->type_user = $user_type_title;
        }

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 6.4  Update status of A User  ********************************


    public function update_status(Request $request,$id)
    {
        
        $status = $this->validate_var(@$request->status,'');
        
        $user = \App\User::where('id',$id)->update([


            'status' => $status


        ]);

        $result = \App\User::where('id',$id)->get();


        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Status Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Status';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 6.5 Change Password  ********************************


       
    public function changePassword(Request $request, $id)
    {
        


        // *********** Check for required fields ****************


            $validator = Validator::make($request->all(), [
                    'current_password' => 'required',
                    'new_password' => 'required',
                    'confirm_password' => 'required',
            ]);

            if ($validator->errors()->all()) 
            {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;                  
            }


        // *********** Matching the new password and confirm password ****************

            if($request->new_password == $request->confirm_password)
            {
                

                // *********** Matching the old password and Change with new password ****************

         
                if($request->current_password != '' && $request->current_password != null)
                {
                    $current_password            =       $request->current_password;  
                    $password                =       Hash::make($request->new_password);

                    $current_password_data = \App\User::where('id', '=', $id)->get(["id","password"]);            
                    if(Hash::check($current_password, $current_password_data[0]->password))
                    {  
                        \App\User::where('id', $id)->update(['password' => $password]); 

                        $data['status_code']    =   1;
                        $data['status_text']    =   'Success';
                        $data['message']        =   'Password changed successfully.';
                        $data['user_id']        =   $current_password_data[0]->id;
                    } 
                    else 
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';
                        $data['message']        =   'Invalid Current Password';            
                    }   
                }
                else
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';
                    $data['message']        =   'Invalid Data'; 
                }        
            }
            else
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';
                $data['message']        =   'Password Not Mached';
            }
            return $data;
    }



    //*************************** Route No. 6.6  Update A User  ********************************


    public function update_profile(Request $request,$id)
    {
        
        $input=array_filter($request->all()); 

        $user = \App\User::where('id',$id)->update($input);

        $result = \App\User::where('id',$id)->get();

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Profile Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Profile';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }
}


    