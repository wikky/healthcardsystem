<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\trait_functions;
use Validator;
use Auth;
use App;
use Hash;
use File;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class PatientController extends Controller
{
    use trait_functions;

    
    //*************************** Route No. 2.1  Create Option  ********************************


    public function register_patient(Request $request)
    {
        
            // *********** Check for required fields ****************


            $validator=Validator::make($request->all(), [

                'patient_name'   => 'required',
                'card_no'        => 'required',
                'cr_no'          => 'required',
                'ward'           => 'required',
            ]);

            if($validator->errors()->all())
            {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = $validator->errors()->first();
                return $data;    
            }


            $cr_no = $this->validate_var(@$request->cr_no, '');

            $patient_status = @\App\PatientDetail::where('cr_no',$cr_no)->count();

            if($patient_status > 0)
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Patient Already Registered';
                return $data;
            }


            // *********** Store data into patient_details table ****************


            $patient = new App\PatientDetail;
            $patient->patient_name = $this->validate_var(@$request->patient_name,'');
            $patient->card_no = $this->validate_var(@$request->card_no,'');
            $patient->cr_no = $this->validate_var(@$request->cr_no,'');
            $patient->ward = $this->validate_var(@$request->ward,'');
            $patient->save();

            $patient_id = $patient['id'];


            $patient_data = @\App\PatientDetail::where('id',$patient_id)->get();


            if($patient_data != '')
            {
                $data['status_code']    =  1;
                $data['status_text']    =  'Success';
                $data['message']        =  'Patient Registered Successfully';
                $data['data']           =  $patient_data;
            }
            else
            {
                $data['status_code']    =  0;
                $data['status_text']    =  'Failed';
                $data['message']        =  'Unable To Registered Patient';
                $data['data']           =  [];
            } 
        
        return $data;                 
    }




    //*************************** Route No. 1.2  List All Patients  ********************************


    public function get_patient_list()
    {
        
        $per_page = $this->validate_var(@$_GET['per_page'],20);
        $order = $this->validate_var(@$_GET['order'],'DESC');
        $order_by = $this->validate_var(@$_GET['order_by'],'created_at');
        $cr_no = $this->validate_var(@$_GET['cr_no'],'');
        $health_card_scheme = $this->validate_var(@$_GET['health_card_scheme'],'');
       
        $patients = \App\PatientDetail::where('id','<>',0);

        if($cr_no != '' && $cr_no != null)
        {
            
            $patients = $patients->where('cr_no',$cr_no);
        }

        if($health_card_scheme != '' && $health_card_scheme != null)
        {
            
            $patients = $patients->where('health_card_scheme',$health_card_scheme);
        }

        $patients = $patients->orderBy($order_by,$order);

        $result = $patients->paginate($per_page)->appends(request()->query());

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Patient List Fetched Successfully';
            $data['data']      =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Result Not Found';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }


    //*************************** Route No. 2.3  Update Patient Detail  ********************************

    
    public function update_patient_detail(Request $request,$id)
    {
          
        $input=array_filter($request->all());
        $patient = \App\PatientDetail::where('cr_no',$id)->update($input);


        $result = \App\PatientDetail::where('cr_no',$id)->get();

        if(sizeof($result) > 0)
        {
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';             
            $data['message']        =   'Patient Detail Updated Successfully';
            $data['data']           =   $result;  
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   'Unable To Update Patient Detail';
            $data['data']      =   [];  
        }
                                  
        return $data;
    }
}