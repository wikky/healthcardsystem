<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabTestDetail extends Model
{
    protected $fillable = [

        'date','department','test','receipt_no','amount','memo_no','tag','patient_cr_no'

    ];
}
