<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabTest extends Model
{
    protected $fillable = [

        'department','title','identifier','amount','status'
    ];
}
