<?php
namespace App\Traits;
use App;

trait trait_functions
{
    protected function trait_functions($name)
    {
        return $name;
    }


	public function validate_var($var , $default)
	{
	    if($var == '' || $var == null || $var == 'null' || $var == ' ')
	    {
	        return $default;
	    }
	    return $var;
	}
}	
