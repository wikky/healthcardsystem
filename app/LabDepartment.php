<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabDepartment extends Model
{
    protected $fillable = [

        'title','identifier','status'
    ];
}
