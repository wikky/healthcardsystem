<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthCard extends Model
{
    protected $fillable = [

        'title','identifier','status'
    ];
}
