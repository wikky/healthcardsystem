<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'cors'], function() {


    Route::post('v1/user/register','api\UserController@register');                            //Route No. 6.1
    Route::post('v1/user/login','api\UserController@login');                                  //Route No. 6.4


    //Route::group(['middleware' => 'checkauth'], function() {


        //************************************************ Routes for PatientController Starts 1.0 ******************

        Route::post('v1/patient','api\PatientController@register_patient');                  //Route No. 1.1
        Route::get('v1/patients','api\PatientController@get_patient_list');                  //Route No. 1.2
        Route::put('v1/patient/{id}','api\PatientController@update_patient_detail');         //Route No. 1.3

        //************************************************ Routes for PatientController Ends *************************


        //************************************************ Routes for HealthCardSchemeController Starts 2.0 ******************

        Route::post('v1/health/card','api\HealthCardSchemeController@generate_healthcard');           //Route No. 2.1
        Route::get('v1/health/cards','api\HealthCardSchemeController@get_health_card_list');          //Route No. 2.2
        Route::put('v1/health/card/{id}','api\HealthCardSchemeController@update_health_card');        //Route No. 2.3
        Route::delete('v1/health/card/{id}','api\HealthCardSchemeController@destroy');                //Route No. 2.4
        Route::put('v1/health/card/status/{id}','api\HealthCardSchemeController@update_health_card_status'); //Route No. 2.5

        //************************************************ Routes for HealthCardSchemeController Ends *************************



        //************************************************ Routes for LabDepartmentController Starts 3.0 ******************

        Route::post('v1/lab/department','api\LabDepartmentController@generate_lab_department');            //Route No. 3.1
        Route::get('v1/lab/departments','api\LabDepartmentController@get_lab_department_list');            //Route No. 3.2
        Route::put('v1/lab/department/{id}','api\LabDepartmentController@update_lab_department');          //Route No. 3.3
        Route::delete('v1/lab/department/{id}','api\LabDepartmentController@destroy');                     //Route No. 3.4
        Route::put('v1/lab/department/status/{id}','api\LabDepartmentController@update_lab_department_status'); //Route No. 3.5

        //************************************************ Routes for LabDepartmentController Ends *************************



        //************************************************ Routes for LabTestController Starts 4.0 ******************

        Route::post('v1/lab/test','api\LabTestController@generate_lab_test');            //Route No. 4.1
        Route::get('v1/lab/tests','api\LabTestController@get_lab_test_list');            //Route No. 4.2
        Route::put('v1/lab/test/{id}','api\LabTestController@update_lab_test');          //Route No. 4.3
        Route::delete('v1/lab/test/{id}','api\LabTestController@destroy');               //Route No. 4.4
        Route::put('v1/lab/test/status/{id}','api\LabTestController@update_lab_test_status'); //Route No. 4.5

        //************************************************ Routes for LabTestController Ends *************************



        //************************************************ Routes for LabTestBillingController Starts 5.0 ******************

        Route::post('v1/lab/test/bill','api\LabTestBillingController@generate_lab_test_memo');              //Route No. 5.1
        Route::get('v1/lab/test/last_bill','api\LabTestBillingController@get_last_bill_no');                //Route No. 5.2
        Route::get('v1/lab/test/last_req','api\LabTestBillingController@get_last_requisition_no');          //Route No. 5.3
        Route::get('v1/lab/test/req/detail/{id}','api\LabTestBillingController@get_requisition_detail');    //Route No. 5.4
        Route::get('v1/lab/test/bills','api\LabTestBillingController@get_test_bill_list');                  //Route No. 5.5
        Route::get('v1/lab/test/bill/date/{date}','api\LabTestBillingController@get_test_bill_date');       //Route No. 5.6
        
        //************************************************ Routes for LabTestBillingController Ends *************************



        //************************************************ Routes for UserController Starts 6.0 ******************

        Route::get('v1/users','api\UserController@get_list');                                     //Route No. 6.2
        Route::put('v1/user/update/status/{id}','api\UserController@update_status');              //Route No. 6.3
        Route::post('v1/user/change/password/{id}','api\UserController@changePassword');          //Route No. 6.5
        Route::put('v1/user/update/{id}','api\UserController@update_profile');                    //Route No. 6.6

        //************************************************ Routes for UserController Ends *************************


        //************************************************ Routes for UserTypeController Starts 7.0 ******************

        Route::post('v1/user/type','api\UserTypeController@generate_user_type');              //Route No. 7.1
        Route::get('v1/user/types','api\UserTypeController@get_user_type_list');              //Route No. 7.2
        Route::put('v1/user/type/{id}','api\UserTypeController@update_user_type');            //Route No. 7.3
        Route::delete('v1/user/type/{id}','api\UserTypeController@destroy');                     //Route No. 7.4
        Route::put('v1/user/type/status/{id}','api\UserTypeController@update_user_type_status'); //Route No. 7.5
    
        //************************************************ Routes for UserTypeController Ends *************************



        //************************************************ Routes for DashboardController Starts 8.0 ******************

        Route::get('v1/dashboard','api\DashboardController@dashboard_data');                //Route No. 8.1
           
        //************************************************ Routes for DashboardController Ends *************************



        //************************************************ Routes for AdminController Starts 9.0 ******************

        Route::get('v1/admin/health/cards','api\AdminController@get_health_card_list');         //Route No. 9.1
        Route::get('v1/admin/lab/departments','api\AdminController@get_lab_department_list');   //Route No. 9.2
        Route::get('v1/admin/lab/tests','api\AdminController@get_lab_test_list');               //Route No. 9.3
        Route::get('v1/admin/user/types','api\AdminController@get_user_type_list');             //Route No. 9.4

        //************************************************ Routes for AdminController Ends *************************

    //});

});
