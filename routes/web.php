<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('login', function () {
    return view('admin.login');
});
Route::get('register', function () {
    return view('admin.register');
});
// Route::group(['middleware' => 'checkauth'], function() {
    Route::get('dashboard', function () {
        return view('admin.dashboard');
    });

    Route::get('add_patient', function () {
        return view('admin.add_patient');
    });
    Route::get('patient_listing', function () {
        return view('admin.patient_listing');
    });
    Route::get('patient_list', function () {
        return view('admin.patient_list');
    });
    Route::get('edit_patient', function () {
        return view('admin.edit_patient');
    });
    Route::get('health_card_listing', function () {
        return view('admin.health_card_listing');
    });
    Route::get('users_listing', function () {
        return view('admin.users_listing');
    });
    Route::get('user_type_listing', function () {
        return view('admin.user_type_listing');
    });
    Route::get('lab_test_listing', function () {
        return view('admin.lab_test_listing');
    });
    Route::get('department_list', function () {
        return view('admin.department_list');
    });
    Route::get('profile', function () {
        return view('admin.profile');
    });
    Route::get('change_password', function () {
        return view('admin.change_password');
    });
    Route::get('bill', function () {
        return view('admin.bill');
    });
    Route::get('admin_patient_list', function () {
        return view('admin.admin_patient_list');
    });
    Route::get('patient_detail', function () {
        return view('admin.patient_detail');
    });
    Route::get('investigation', function () {
        return view('admin.investigation');
    });
    Route::get('tests_list', function () {
        return view('admin.tests_list');
    });
// });