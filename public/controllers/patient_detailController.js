app.controller("patient_detailController", function (
    $scope,
    $http,
    toastr,
    $window
) {
    $scope.patient = JSON.parse(localStorage.getItem("patient_detail"));
    console.log($scope.patient);

    //?get health card api

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/health/cards",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.health_card_list = data.data.data;
            console.log($scope.health_card_list);

            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });
});
