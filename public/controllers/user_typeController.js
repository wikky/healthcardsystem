app.controller("user_typeController", function ($scope, $http, toastr, $window) {
    //?getting list of departments api

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/admin/user/types",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.user_type_list = data.data.data;
            console.log($scope.user_type_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //?add new user type function

    $scope.add_new_user_type = function () {
        if ($("#add_user_type_title").val() == "") {
            toastr.error("Enter User Type Title", "Error");
            return false;
        }
        user_type_data = {
            title: $("#add_user_type_title").val()
        };
        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/user/type",
            data: user_type_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.new_user_type_data = data.data;
                console.log($scope.new_user_type_data);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?get user_type function

    $scope.get_user_type = function (user_type) {
        console.log(user_type);
        $scope.user_type_id = user_type.id;
        $scope.user_type_detail = user_type;
    };
    $scope.edit_user_type = function () {
        if ($("#edit_user_type_title").val() == "") {
            toastr.error("Enter User Type Title", "Error");
            return false;
        }
        user_type_edit = {
            title: $("#edit_user_type_title").val()
        };
        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/user/type/" + $scope.user_type_id,
            data: user_type_edit,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_user_type_data = data.data;
                console.log($scope.updated_user_type_data);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?change department status

    $scope.change_status = function (user_type) {
        console.log(user_type);
        user_type_status = {
            status: user_type.status
        };
        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/user/type/status/" + user_type.id,
            data: user_type_status,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_status = data.data;
                console.log($scope.updated_status);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
