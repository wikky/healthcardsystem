app.controller("loginController", function ($scope, $http, toastr, $window) {
    //?get user type
    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/user/types",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.user_type_list = data.data.data;

            console.log($scope.user_type_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //?login user
    $scope.login = function () {
        if ($("#password").val() == "") {
            toastr.error("Enter First Password");
            return false;
        } else if ($("#email").val() == "") {
            toastr.error("Enter email");
            return false;
        }
        login_data = {
            email: $("#email").val(),
            password: $("#password").val()
        };
        console.log(login_data);
        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/user/login",
            data: login_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.logined_user = data.data;

                console.log($scope.logined_user);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    localStorage.setItem(
                        "logined_user",
                        JSON.stringify($scope.logined_user[0])
                    );
                    setTimeout(function () {
                        if ($scope.logined_user[0].user_type == 'admin') {
                            $window.location.href = Local_URL + "/dashboard";
                        } else if ($scope.logined_user[0].user_type == 'doctor') {
                            $window.location.href = Local_URL + "/patient_list";
                        } else if ($scope.logined_user[0].user_type == 'lab_assistant') {
                            $window.location.href = Local_URL + "/bill";
                        } else if ($scope.logined_user[0].user_type == 'operator') {
                            $window.location.href = Local_URL + "/patient_listing";
                        } else {
                            $window.location.href = Local_URL + "/dashboard";
                        }

                    });
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
