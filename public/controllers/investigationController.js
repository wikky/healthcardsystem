app.controller("investigationController", function ($scope, $http, toastr, $window) {

    $scope.get_user_detail = function () {
        $scope.req_no = $("#get_user").val();
        if ($scope.req_no == "") {
            toastr.error("Enter requsition number first", "Error");
            return false;
        }
        console.log($scope.req_no);
        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/lab/test/req/detail/" + $scope.req_no,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.req_detail_list = data.data;
                $scope.patient_detail = data.patient_detail;
                $scope.pat = $scope.patient_detail[0];
                console.log($scope.req_detail_list);
                console.log($scope.patient_detail);
                if (data.status_code == 1) {
                    // toastr.success(data.message, "Success");
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
    $scope.print_investigation = function () {

        var content = document.getElementById('content_div').innerHTML;
        var mywindow = window.open();

        mywindow.document.write(`<html><head> <style>
        table{
            width:100%;
            display: table;
            border-collapse: collapse;
        }
        
        table tr th,table tr td{
            text-align:left;
            padding:1rem;
            font-family:proxima-semibold;
            color:#000;
            font-size:1rem;
        }
        </style> 
        </head>
        <body style='padding:3rem'><br> 
        <h3 style='text-align:center'>IGMC SHIMLA</h3>
    <table style="width:100%">
        <tr>
            <td>CR No.</td>
             <td>` + $scope.pat.cr_no + `</td>
             <td>Patient Name</td>
             <td>` + $scope.pat.patient_name + `</td>
        </tr>
      
        <tr>
            <td>Age</td>
             <td>` + $scope.pat.age + ` Yrs</td>
              <td>Sex</td>
             <td style='text-transform: uppercase;'>` + $scope.pat.sex + `</td>
              
        </tr>
     
        <tr>
         <td>Bill No.</td>
             <td>` + $scope.req_detail_list[0].memo_no + `</td>
            <td>Req No.</td>
             <td>` + $scope.req_detail_list[0].req_no + `</td>
        </tr>
         <tr>
         <td>Address</td>
             <td>` + $scope.pat.address + `</td>
           
        </tr>
    </table> 
     <hr>
     <h3 style='text-align:center;margin:1rem 0'>Test Details</h3>
    ` + content +
            `
 <p><b>Diagnosis : </b>________________________________<br><br>
 <b>Referring Consultant : </b>________________________ <br><br>
 <b>Designation : </b>__________________________________ <br><br>
 <b>Remarks : </b>
 </p>
   
    
    
    </body></html>`);

        mywindow.document.close();
        mywindow.focus()
        mywindow.print();
    }


})
