app.controller("dashboardController", function ($scope, $http, toastr, $window) {
    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/dashboard",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.dashboard_data = data;

            console.log($scope.dashboard_data);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });
})
