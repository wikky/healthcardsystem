app.controller("admin_patient_listController", function (
    $scope,
    $http,
    toastr,
    $window
) {

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/patients",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.patient_list = data.data.data;
            console.log($scope.patient_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //? get patient detail api
    $scope.get_patient = function (patient) {
        console.log(patient);
        localStorage.setItem("patient_detail", JSON.stringify(patient));
        $window.location.href = Local_URL + "/patient_detail";
    };

    //?getting list of health cards api

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/health/cards",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.health_card_list = data.data.data;
            console.log($scope.health_card_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });
    //?user_type filter
    $scope.change_health_card = function () {
        $scope.health_card = $("#filter_type").val();
        console.log($scope.health_card);
        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/patients?health_card_scheme=" + $scope.health_card,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.patient_list = data.data.data;
                console.log($scope.patient_list);
                if (data.status_code == 1) {
                    // toastr.success(data.message, "Success");
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
