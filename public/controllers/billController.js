app.controller("billController", function ($scope, $http, toastr, $window) {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    $scope.current_date = today;

    $scope.get_user_detail = function () {
        $scope.cr_no = $("#get_user").val();
        if ($scope.cr_no == "") {
            toastr.error("Enter CR Number first", "error");
            return false;
        }
        console.log($scope.cr_no);
        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/patients?cr_no=" + $scope.cr_no,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.patient_list = data.data.data;

                console.log($scope.patient_list);
                if ($scope.patient_list) {
                    $scope.pat = $scope.patient_list[0];
                }

                if (data.status_code == 1) {
                    // toastr.success(data.message, "Success");
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };


    $("#print_table tbody").on("click", ".delete_me", function () {

        console.log($scope.total_amount);
        last_amount = $scope.amount_Array[$scope.amount_Array.length - 1];
        for (var i = 0; i < $scope.amount_Array.length; i++) {
            $scope.amount_Array.splice($scope.amount_Array.length - 1, 1);
        }
        console.log($scope.amount_Array);
        console.log(last_amount);
        $scope.total_amount -= last_amount;
        console.log($scope.total_amount);
        $(this)
            .closest("tr")
            .remove();
    });




    $scope.total_amount = 0;
    $scope.amount_Array = [];


    $scope.add_row = function () {
        if (!$scope.amt) {
            $("#get_test").val('');
            $("#get_department").val('');
            toastr.error('Select test first', 'Error');
            return false;
        }

        if ($scope.test_Array) {
            for (var i = 0; i < $scope.test_Array.length; i++) {
                if ($scope.test_Array[i] == $scope.test_id) {
                    $("#get_test").val('');
                    $("#get_department").val('');
                    toastr.error('Test is already added', 'Error', 'Error');
                    return false;
                }
            }
            $scope.test_Array.push($scope.test_id);
            $scope.department_Array.push($scope.department);

            console.log($scope.test_Array);
            console.log($scope.department_Array);
            $("#get_test").val('');
            $("#get_department").val('');
        }
        $("#get_test").val('');
        $("#get_department").val('');
        console.log($scope.amt);
        console.log($scope.dapartment_value);
        console.log($scope.test_value);
        $scope.amount_Array.push(parseInt($scope.amt));
        $scope.total_amount += parseInt($scope.amt);;
        console.log($scope.amount_Array);
        var markup =
            `<tr>
            <td>
                 ` +
            $scope.dapartment_value +
            `
                </td>
              <td>
                  ` +
            $scope.test_value +
            `
                </td>
                <td>
                  ` +
            $("#amount").val() +
            `
                </td>
            </tr>`;
        $("#print_table tbody").append(markup);

        $("#get_test").val('');
        $("#get_department").val('');



    };

    //?getting list of departments api
    $scope.get_dept = function () {

        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/lab/departments",
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.department_list = data.data.data;
                console.log($scope.department_list);
                if (data.status_code == 1) {
                    // toastr.success(data.message, "Success");
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });

    }
    $scope.get_dept();

    //?get test title api
    $scope.test_Array = [];
    $scope.change_test = function () {

        $scope.test_id = $("#get_test")
            .find(":selected")
            .attr("data-id");
        console.log($scope.test_id);

        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/lab/tests?test_id=" + $scope.test_id,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                if (data) {
                    $scope.amount = data.data.data[0].amount;
                    $scope.amt = data.data.data[0].amount;
                    console.log($scope.amount);
                }
                if (data.status_code == 1) {
                    // $scope.get_dept();

                    // toastr.success(data.message, "Success");
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?get department title api
    $scope.department_Array = [];
    $scope.change_department = function () {

        $scope.department = $("#get_department")
            .find(":selected")
            .attr("data-id");
        console.log($scope.department);


        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/lab/tests?department=" + $scope.department,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.lab_test_list = data.data.data;

                console.log($scope.lab_test_list);
                if (data.status_code == 1) {
                    // toastr.success(data.message, "Success");
                    $scope.test_value = '';

                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //? getting last bill number

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/lab/test/last_bill",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.last = data.last_bill_no;
            console.log($scope.last);
            $scope.last_bill = parseInt($scope.last) + 1;
            console.log($scope.last_bill);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });


    //? getting last requsition number

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/lab/test/last_req",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.last_req = data.last_requisition_no;
            console.log($scope.last_req);
            $scope.last_requsition = parseInt($scope.last_req) + 1
            console.log($scope.last_requsition);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });




    $scope.print_bill = function () {

        var content = document.getElementById('content_div').innerHTML;
        var mywindow = window.open();

        mywindow.document.write(`<html><head> <style>
        table{
            width:100%;
            display: table;
            border-collapse: collapse;
        }
        
        table tr th,table tr td{
            text-align:left;
            padding:1rem;
            font-family:proxima-semibold;
            color:#000;
            font-size:1rem;
        }
        </style> 
        </head>
        <body style='padding:3rem'><br> 
        <h3 style='text-align:center'>IGMC SHIMLA</h3>
    <table style="width:100%">
        <tr>
            <td>Date</td>
            <td>` + $scope.pat.date + `</td>
             <td>Patient Name</td>
             <td>` + $scope.pat.patient_name + `</td>
        </tr>
      
        <tr>
            <td>Age</td>
             <td>` + $scope.pat.age + `Yrs</td>
            <td>Sex</td>
             <td style='text-transform: uppercase'>` + $scope.pat.sex + `</td>
        </tr>
     
        <tr>
        <td>Bill No.</td>
             <td>` + $scope.last_bill + `</td>
            <td>Req No.</td>
             <td>` + $scope.last_requsition + `</td>
        </tr>
    </table> 
     <hr>
    ` + content +
            `
    <h4 style='text-align:right'>Total:` + $scope.total_amount + `</h4>
   
    
    
    </body></html>`);

        mywindow.document.close();
        mywindow.focus()
        mywindow.print();
    }

    $scope.save_data = function () {
        save = {
            'cr_no': $scope.cr_no,
            'department': $scope.department_Array,
            'test': $scope.test_Array,
            'amount': $scope.amount_Array,
            'bill_no': $scope.last_bill,
            'date': $scope.current_date,
            'req_no': $scope.last_requsition
        }
        console.log(save);

        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/lab/test/bill",
            data: save,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.generated_bill = data;

                console.log($scope.generated_bill);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    $('.saav_me').css('display', 'none');
                    $('.print_me').css('display', 'block');
                } else {
                    $('.print_me').css('display', 'none');
                    $('.saav_me').css('display', 'block');
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });

    }

});
