app.controller("edit_patientController", function (
    $scope,
    $http,
    toastr,
    $window
) {
    $scope.cr_num = $window.location.href.substr($window.location.href.lastIndexOf('?') + 1);

    //?getting patient

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/patients?cr_no=" + $scope.cr_num,
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.patient_list = data.data.data;
            $scope.patient = $scope.patient_list[0];
            console.log($scope.patient);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //?getting current date

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = dd + '/' + mm + '/' + yyyy;
    $scope.current_date = today;
    //?get health card api

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/health/cards",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.health_card_list = data.data.data;
            console.log($scope.health_card_list);

            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //? edit patient function

    $scope.edit_patient = function () {
        if ($("#patient_name").val() == "") {
            toastr.error("Enter patient name", "Error");
            return false;
        } else if ($("#card_number").val() == "") {
            toastr.error("Enter card number", "Error");
            return false;
        } else if ($("#cr_number").val() == "") {
            toastr.error("Enter CR number", "Error");
            return false;
        } else if ($("#ward_name").val() == "") {
            toastr.error("Enter ward name", "Error");
            return false;
        }

        patient_data = {
            patient_name: $("#patient_name").val(),
            card_no: $("#card_number").val(),
            cr_no: $("#cr_number").val(),
            ward: $("#ward_name").val(),
            address: $("#address").val(),
            approved_amount: $("#approved_amount").val(),
            case_no: $("#case_number").val(),
            date: $scope.current_date,
            extended_approved_amount: $("#extend_approved_amount").val(),
            extended_case_no: $("#extnd_case_no").val(),
            health_card_scheme: $scope.logined_user.health_card_scheme,
            medco_name: $scope.logined_user.first_name + '' + $scope.logined_user.last_name,
            mobile: $("#mobile").val(),
            package_code: $("#package_code").val(),
            package_name: $("#package_name").val(),
            previous_package_detail: $("#prev_package_detail").val(),
            whether_package_changed: $("#package_changed").val(),
            whether_package_extend: $("#package_extended").val(),
            status: "0",
            sex: $('#sex').val(),
            age: $('#age').val()
        };
        console.log(patient_data);

        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/patient/" + $scope.patient.cr_no,
            data: patient_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.patient_result = data.data;
                console.log($scope.patient_result);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.href = Local_URL + "/patient_listing";
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    $scope.print_data = function () {

        var content = document.getElementsByClassName('noprint').innerHTML;
        var mywindow = window.open();

        mywindow.document.write(`<html><head> <style>
        table{
            width:90%;
            display: table;
            border-collapse: collapse;
        }
         table tr th
          {
            width:40%;
            border: 1px solid #464646;
            padding:1rem;
            font-family:proxima-semibold;
            color:#000;
            font-size:1rem;
        }
        table tr th,table tr td{
            border:1px solid #000;
            padding:1rem;
            font-family:proxima-semibold;
            color:#000;
            font-size:1rem;
        }
        </style> 
        </head>
        <body style='padding:3rem'><br> 
        <center> <h1 style='text-transform:capitalize'> ` + $scope.logined_user.health_card_scheme + ` </h1><table class='table' style='text-align:left;'>
        <tr>
            <th>Patient Name</th>
            <td style='text-transfomr:capitalize'>` + $("#patient_name").val() + `</td>
        </tr>
        <tr>
            <th>Card No.</th>
            <td> ` + $("#card_number").val() + ` </td>
        </tr>
        <tr>
            <th>CR No.</th>
            <td> ` + $("#cr_number").val() + ` </td>
        </tr>
        <tr>
            <th>Ward</th>
            <td> ` + $("#ward_name").val() + ` </td>
        </tr>
        <tr>
            <th>Phone No.</th>
            <td> ` + $("#mobile").val() + ` </td>
        </tr>
          <tr>
             <th> Address </th> <td> ` + $("#address").val() + ` </td>
             </tr>
        <tr>
            <th>Package Name</th>
            <td> ` + $("#package_name").val() + ` </td>
        </tr>
         <tr>
             <th> Package Code </th> <td> ` + $("#package_code").val() + ` </td>
              </tr>
        <tr>
            <th>Medico Name & Signature</th>
            <td> ` + $scope.logined_user.first_name + '' + $scope.logined_user.last_name + ` </td>
        </tr>
        <tr>
            <th>Date</th>
            <td> ` + $scope.current_date + ` </td>
        </tr>
        <tr>
            <th>Package Amount</th>
            <td> ` + $("#approved_amount").val() + ` </td>
        </tr>
        <tr>
            <th>Case NO.</th>
            <td> ` + $("#case_number").val() + ` </td>
        </tr>
      

    </table>  </body></html>`);

        mywindow.document.close();
        mywindow.focus()
        mywindow.print();
    }
    $scope.print_data_basic = function () {

        var content = document.getElementsByClassName('noprint').innerHTML;
        var mywindow = window.open();

        mywindow.document.write(`<html><head> <style>
        table{
            width:100%;
            display: table;
            border-collapse: collapse;
        }
         table tr th
          {
            width:40%;
           border: 1px solid #464646;
            padding:.3rem;
             font-family:proxima-semibold;
            color:#000;
            font-size:1rem;
        }
        table tr td{
           border: 1px solid #464646;
            padding:.3rem;
             font-family:proxima-semibold;
            color:#000;
            font-size:1rem;
        }
        #pmjy tr td{
            border:none;
            padding:1rem 0;
            font-size:.9rem;
        }
        </style> 
        </head>
        <body style='padding:3rem'><br> 
        <h1 style='margin-top:3rem; text-align:center;text-transform:capitalize'> ` + $scope.logined_user.health_card_scheme + ` </h1>

        <table class='table' style='text-align:left;'>
       
        <tr>
            <th style='width:10%'>Patient Name</th>
            <th style='width:10%'>Card Number</th>
            <th style='width:10%'>CR No.</th>
            <th style='width:10%'>Ward</th>
            <th style='width:10%'>Date</th>
            <th style='width:10%'>Package Code</th>
            <th style='width:10%'>Package Amount</th>
           
        </tr>
        <tr>
            <td> ` + $("#patient_name").val() + ` </td>
            <td> ` + $("#card_number").val() + ` </td>
            <td> ` + $("#cr_number").val() + ` </td>
            <td> ` + $("#ward_name").val() + ` </td>
            <td> ` + $scope.current_date + ` </td>
            <td> ` + $("#package_code").val() + ` </td>
            <td> ` + $("#approved_amount").val() + ` </td>
        </tr>
        
        

    </table> 
         <p style='margin:1rem 0;'>Note. It is mandatory to use medicines and surgical consumables available in hospital under 'Free Drug Policy' of
        the goverment under
        National Health Mission. Nursing staff shall send duly signed requisition form before 3 P.M to the office of
        Medical Officer (stores) IGMC, Shimla.
    </p>
     <h3>Send the patient to counter number 50 before buying the medicines from market</h3>
    <table style="width:48%;float:left;margin-bottom:1rem">
        <tr>
            <th style="width:10%">Sr.No.</th>
            <th style="width:70%">Drug / Surgical Consumables</th>
            <th style="width:20%">Quantity / No. of Units</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>4</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>5</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>6</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>7</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>8</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>9</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>10</td>
            <td></td>
            <td></td>
        </tr>

    </table>
    <table style="width:48%;float:right; margin-bottom:1rem">
        <tr>
            <th style="width:10%">Sr.No.</th>
            <th style="width:70%">Drug / Surgical Consumables</th>
            <th style="width:20%">Quantity / No. of Units</th>
        </tr>
        <tr>
            <td>11</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>12</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>13</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>14</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>15</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>16</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>17</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>18</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>19</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>20</td>
            <td></td>
            <td></td>
        </tr>

    </table>
   <table id='pmjy' style='margin-top:1rem;'>
        <tr>
            <td style="width:60%">It is certified that thses medicines are usefull for the treatment of
                patient</td>
            <td style="width:40%;text-align:right">Signature & Stamp of treating Doctor  </td>
        </tr>
        <tr>
            <td style="width:60%">It is certified that thses medicines are not available in the hospital</td>
            <td style="width:40%;text-align:right">Signature of Staff Nurse / Ward Sister </td>
        </tr>
        <tr>
            <td style="width:60%">Please collect these medicines from PMJY Center</td>
            <td style="width:40%;text-align:right">Signature &
                Stamp of Medical officer(Stores)</td>
        </tr>
    </table>
    </body></html>`);

        mywindow.document.close();
        mywindow.focus()
        mywindow.print();
    }


});
