app.controller("patient_listController", function (
    $scope,
    $http,
    toastr,
    $window
) {
    $scope.get_cr_patient = function () {
        if ($('#get_cr').val() == '') {
            toastr('Enter CR NUmber', 'Error');
            return false;
        }
        $scope.cr_num = $('#get_cr').val();
        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/patients?cr_no=" + $scope.cr_num,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.patient_list = data.data.data;
                console.log($scope.patient_list);
                if (data.status_code == 1) {
                    // toastr.success(data.message, "Success");
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    }

    //? get patient detail api
    $scope.get_patient = function (patient) {
        console.log(patient);
        $scope.patient_detail = patient;
    };

    //?add patient function

    $scope.add_patient = function () {
        if ($("#patient_name").val() == "") {
            toastr.error("Enter patient name", "Error");
            return false;
        } else if ($("#card_number").val() == "") {
            toastr.error("Enter card number", "Error");
            return false;
        } else if ($("#cr_number").val() == "") {
            toastr.error("Enter CR number", "Error");
            return false;
        } else if ($("#ward_name").val() == "") {
            toastr.error("Enter ward name", "Error");
            return false;
        }

        patient_data = {
            patient_name: $("#patient_name").val(),
            card_no: $("#card_number").val(),
            cr_no: $("#cr_number").val(),
            ward: $("#ward_name").val()
        };
        console.log(patient_data);

        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/patient",
            data: patient_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.patient_result = data.data;
                console.log($scope.patient_result);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");

                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
    $scope.update_patient = function () {
        if ($("#edit_patient_name").val() == "") {
            toastr.error("Enter patient name", "Error");
            return false;
        } else if ($("#edit_card_number").val() == "") {
            toastr.error("Enter card number", "Error");
            return false;
        } else if ($("#edit_cr_number").val() == "") {
            toastr.error("Enter CR number", "Error");
            return false;
        } else if ($("#edit_ward_name").val() == "") {
            toastr.error("Enter ward name", "Error");
            return false;
        }

        patient_data_edit = {
            patient_name: $("#edit_patient_name").val(),
            card_no: $("#edit_card_number").val(),
            cr_no: $("#edit_cr_number").val(),
            ward: $("#edit_ward_name").val()
        };
        console.log(patient_data_edit);

        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/patient/" + $scope.patient_detail.id,
            data: patient_data_edit,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_patient = data.data;
                console.log($scope.updated_patient);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");

                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
