app.controller("add_patientController", function(
    $scope,
    $http,
    toastr,
    $window
) {
    // add patient function

    $scope.add_patient = function() {
        if ($("#patient_name").val() == "") {
            toastr.error("Enter patient name", "Error");
            return false;
        } else if ($("#card_number").val() == "") {
            toastr.error("Enter card number", "Error");
            return false;
        } else if ($("#cr_number").val() == "") {
            toastr.error("Enter CR number", "Error");
            return false;
        } else if ($("#ward_name").val() == "") {
            toastr.error("Enter ward name", "Error");
            return false;
        }

        patient_data = {
            patient_name: $("#patient_name").val(),
            card_no: $("#card_number").val(),
            cr_no: $("#cr_number").val(),
            ward: $("#ward_name").val()
        };
        console.log(patient_data);

        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/patient",
            data: patient_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function(data) {
                $scope.patient_result = data.data;
                console.log($scope.patient_result);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");

                    setTimeout(function() {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function(data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
