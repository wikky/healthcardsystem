var app = angular.module("myApp", ["ngMaterial", "toastr"]);
app.controller("mainController", function($scope, $http, toastr, $window) {
    $scope.logined_user = localStorage.getItem("logined_user");
    if ($scope.logined_user) {
        $scope.logined_user = JSON.parse(localStorage.getItem("logined_user"));
    }
    console.log($scope.logined_user);

    $scope.logout = function() {
        localStorage.clear();
        $window.location.href = Local_URL + "/login";
    };
});
