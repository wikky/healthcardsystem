app.controller("profileController", function($scope, $http, toastr, $window) {
    //?update user
    $scope.update_user = function() {
        if ($("#fname").val() == "") {
            toastr.error("Enter First Name");
            return false;
        } else if ($("#email").val() == "") {
            toastr.error("Enter email");
            return false;
        } else if ($("#password").val() == "") {
            toastr.error("Enter password");
            return false;
        } else if ($("#user_type").val() == "") {
            toastr.error("Select user type");
            return false;
        }
        register_data = {
            first_name: $("#fname").val(),
            last_name: $("#lname").val(),
            email: $("#email").val(),
            mobile: $("#mobile").val(),
            landline_no: $("#land_line").val(),
            user_type: $("#user_type").val(),
            address: $("#address").val()
        };
        console.log(register_data);

        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/user/update/" + $scope.logined_user.id,
            data: register_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function(data) {
                $scope.registered_user = data.data[0];
                localStorage.setItem(
                    "logined_user",
                    JSON.stringify($scope.registered_user)
                );
                console.log($scope.registered_user);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function() {
                        $window.location.reload();
                    });
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function(data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
