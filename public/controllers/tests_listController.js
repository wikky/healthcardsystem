app.controller("tests_listController", function (
    $scope,
    $http,
    toastr,
    $window
) {
    //?getting list of cards api
    $scope.get_list = function () {
        if ($('#date_from').val() != '' || $('#date_to').val() != '') {
            $scope.url = APP_URL + "/api/v1/lab/test/bills?date_from=" + $('#date_from').val() + '&date_to=' + $('#date_to').val()

        } else if ($('#date_from').val() == '' && $('#date_to').val() == '') {
            toastr.error('Select dates first', 'Error')
            return false;
        }

        console.log($scope.url);
        var request = $http({
            method: "GET",
            url: $scope.url,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.tests_list = data.data.data;

                console.log($scope.tests_list);
                if (data.status_code == 1) {
                    $('#date_from').val('');
                    $('#date_to').val('');
                    $scope.tests_list_date = '';
                    // toastr.success(data.message, "Success");
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    }

    $scope.get_date_data = function () {
        if ($('#date').val() == '') {
            toastr.error('Select date first', 'Error')
            return false;
        }

        console.log($scope.url);
        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/lab/test/bill/date/" + $('#date').val(),
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.tests_list_date = data.data;
                console.log($scope.tests_list_date);
                if (data.status_code == 1) {
                    // toastr.success(data.message, "Success");
                    $scope.tests_list = '';
                    $('#date').val('');
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    }

    // //? get user detail
    // $scope.get_user = function (user) {
    //     console.log(user);
    //     $scope.user_detail = user;
    // };

    // //?updat user data api
    // $scope.update_user = function () {
    //     user_data = {
    //         first_name: $("#fname").val(),
    //         last_name: $("#lname").val(),
    //         mobile: $("#mobile").val(),
    //         landline_no: $("#land_line").val(),
    //         address: $("#address").val(),
    //         health_card_scheme: $('#helath_card').val()
    //     };
    //     console.log(user_data);

    //     var request = $http({
    //         method: "PUT",
    //         data: user_data,
    //         url: APP_URL + "/api/v1/user/update/" + $scope.user_detail.id,
    //         headers: {
    //             "Content-Type": "application/json"
    //         }
    //     });
    //     request
    //         .success(function (data) {
    //             $scope.updated_user = data.data.data;
    //             console.log($scope.updated_user);
    //             if (data.status_code == 1) {
    //                 toastr.success(data.message, "Success");
    //                 setTimeout(function () {
    //                     $window.location.reload();
    //                 }, 800);
    //             } else {
    //                 toastr.error(data.message, "Error");
    //                 return false;
    //             }
    //         })
    //         .error(function (data, status, headers, config) {
    //             toastr.error(data.message, "Error");
    //         });
    // };

    // $scope.change_status = function (user) {
    //     data = {
    //         status: user.status
    //     };
    //     console.log(data);

    //     var request = $http({
    //         method: "PUT",
    //         url: APP_URL + "/api/v1/user/update/status/" + user.id,
    //         data: data,
    //         headers: {
    //             "Content-Type": "application/json"
    //         }
    //     });
    //     request
    //         .success(function (data) {
    //             $scope.updated_status = data.data.data;
    //             console.log($scope.updated_status);
    //             if (data.status_code == 1) {
    //                 toastr.success(data.message, "Success");
    //                 setTimeout(function () {
    //                     $window.location.reload();
    //                 }, 800);
    //             } else {
    //                 toastr.error(data.message, "Error");
    //                 return false;
    //             }
    //         })
    //         .error(function (data, status, headers, config) {
    //             toastr.error(data.message, "Error");
    //         });
    // };

    // //?getting list of departments api

    // var request = $http({
    //     method: "GET",
    //     url: APP_URL + "/api/v1/user/types",
    //     headers: {
    //         "Content-Type": "application/json"
    //     }
    // });
    // request
    //     .success(function (data) {
    //         $scope.user_type_list = data.data.data;
    //         console.log($scope.user_type_list);
    //         if (data.status_code == 1) {
    //             // toastr.success(data.message, "Success");
    //         } else {
    //             toastr.error(data.message, "Error");
    //             return false;
    //         }
    //     })
    //     .error(function (data, status, headers, config) {
    //         toastr.error(data.message, "Error");
    //     });

    // //?user_type filter
    // $scope.change_user_type = function () {
    //     $scope.user_type = $("#filter_type").val();
    //     console.log($scope.user_type);
    //     var request = $http({
    //         method: "GET",
    //         url: APP_URL + "/api/v1/users?user_type=" + $scope.user_type,
    //         headers: {
    //             "Content-Type": "application/json"
    //         }
    //     });
    //     request
    //         .success(function (data) {
    //             $scope.users_list = data.data.data;
    //             console.log($scope.users_list);
    //             if (data.status_code == 1) {
    //                 // toastr.success(data.message, "Success");
    //             } else {
    //                 toastr.error(data.message, "Error");
    //                 return false;
    //             }
    //         })
    //         .error(function (data, status, headers, config) {
    //             toastr.error(data.message, "Error");
    //         });
    // };
    // //?get health card api

    // var request = $http({
    //     method: "GET",
    //     url: APP_URL + "/api/v1/health/cards",
    //     headers: {
    //         "Content-Type": "application/json"
    //     }
    // });
    // request
    //     .success(function (data) {
    //         $scope.health_card_list = data.data.data;
    //         console.log($scope.health_card_list);

    //         if (data.status_code == 1) {
    //             // toastr.success(data.message, "Success");
    //         } else {
    //             toastr.error(data.message, "Error");
    //             return false;
    //         }
    //     })
    //     .error(function (data, status, headers, config) {
    //         toastr.error(data.message, "Error");
    //     });
});
