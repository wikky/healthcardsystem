app.controller("lab_test_listingController", function (
    $scope,
    $http,
    toastr,
    $window
) {
    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/admin/lab/tests",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.lab_test_list = data.data.data;

            console.log($scope.lab_test_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //?change status

    $scope.change_status = function (lab_test) {
        console.log(lab_test);
        lab_test_data = {
            status: lab_test.status
        };
        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/lab/test/status/" + lab_test.id,
            data: lab_test_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_status = data.data;
                console.log($scope.updated_status);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?getting list of departments api

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/lab/departments",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.department_list = data.data.data;
            console.log($scope.department_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //?getting test detail

    $scope.get_test = function (test) {
        console.log(test);
        $scope.test_detail = test;
    };

    //? add new test

    $scope.add_test = function () {
        if ($("#test_title").val() == "") {
            toastr.error("Enter test title", "Error");
            return false;
        } else if ($("#department").val() == "") {
            toastr.error("Select department", "Error");
            return false;
        } else if ($("#amount").val() == "") {
            toastr.error("Enter amount", "Error");
            return false;
        }

        test_data = {
            department: $("#department").val(),
            title: $("#test_title").val(),
            amount: $("#amount").val()
        };
        console.log(test_data);
        // return;
        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/lab/test",
            data: test_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.test_result = data.data;
                console.log($scope.test_result);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");

                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?update test

    $scope.update_test = function () {
        if ($("#edit_test_title").val() == "") {
            toastr.error("Enter test title", "Error");
            return false;
        } else if ($("#edit_department").val() == "") {
            toastr.error("Select department", "Error");
            return false;
        } else if ($("#edit_amount").val() == "") {
            toastr.error("Enter amount", "Error");
            return false;
        }
        edit_test_data = {
            department: $("#edit_department").val(),
            title: $("#edit_test_title").val(),
            amount: $("#edit_amount").val()
        };
        console.log(edit_test_data);

        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/lab/test/" + $scope.test_detail.id,
            data: edit_test_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_test = data.data;
                console.log($scope.updated_test);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");

                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
