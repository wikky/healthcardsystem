app.controller("patient_listingController", function (
    $scope,
    $http,
    toastr,
    $window
) {
    $scope.get_cr_patient = function () {
        if ($('#get_cr').val() == '') {
            toastr('Enter CR NUmber', 'Error');
            return false;
        }
        $scope.cr_num = $('#get_cr').val();
        var request = $http({
            method: "GET",
            url: APP_URL + "/api/v1/patients?cr_no=" + $scope.cr_num,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.patient_list = data.data.data;
                console.log($scope.patient_list);
                if (data.status_code == 1) {
                    // toastr.success(data.message, "Success");
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    }
    //? get patient detail api
    $scope.get_patient = function (patient) {
        console.log(patient);
        // localStorage.setItem("patient_detail", JSON.stringify(patient));
        $window.location.href = Local_URL + "/edit_patient?" + patient;
    };
});
