app.controller("department_listController", function (
    $scope,
    $http,
    toastr,
    $window
) {
    //?getting list of departments api

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/admin/lab/departments",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.department_list = data.data.data;
            console.log($scope.department_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //?add new department function

    $scope.add_new_department = function () {
        if ($("#add_department_title").val() == "") {
            toastr.error("Enter department Title", "Error");
            return false;
        }
        department_data = {
            title: $("#add_department_title").val()
        };
        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/lab/department",
            data: department_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.new_department_data = data.data;
                console.log($scope.new_department_data);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?get department function

    $scope.get_department = function (department) {
        console.log(department);
        $scope.department_id = department.id;
        $scope.department_detail = department;
    };
    $scope.edit_department = function () {
        if ($("#edit_department_title").val() == "") {
            toastr.error("Enter department Title", "Error");
            return false;
        }
        department_edit = {
            title: $("#edit_department_title").val()
        };
        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/lab/department/" + $scope.department_id,
            data: department_edit,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_department_data = data.data;
                console.log($scope.updated_department_data);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?change department status

    $scope.change_status = function (department) {
        console.log(department);
        department_status = {
            status: department.status
        };
        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/lab/department/status/" + department.id,
            data: department_status,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_status = data.data;
                console.log($scope.updated_status);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
