app.controller("registerController", function($scope, $http, toastr, $window) {
    //?get user type
    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/user/types",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function(data) {
            $scope.user_type_list = data.data.data;

            console.log($scope.user_type_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function(data, status, headers, config) {
            toastr.error(data.message, "Error");
        });
    //?register user function
    $scope.register = function() {
        if ($("#fname").val() == "") {
            toastr.error("Enter First Name");
            return false;
        } else if ($("#email").val() == "") {
            toastr.error("Enter email");
            return false;
        } else if ($("#password").val() == "") {
            toastr.error("Enter password");
            return false;
        } else if ($("#user_type").val() == "") {
            toastr.error("Select user type");
            return false;
        }
        register_data = {
            first_name: $("#fname").val(),
            last_name: $("#lname").val(),
            email: $("#email").val(),
            password: $("#password").val(),
            mobile: $("#mobile").val(),
            landline_no: $("#land_line").val(),
            user_type: $("#user_type").val(),
            address: $("#address").val()
        };
        console.log(register_data);
        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/user/register",
            data: register_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function(data) {
                $scope.registered_user = data.data;

                console.log($scope.registered_user);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function() {
                        $window.location.href = Local_URL + "/login";
                    });
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function(data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
