app.controller("change_passwordController", function(
    $scope,
    $http,
    toastr,
    $window
) {
    $scope.change_password = function() {
        if ($("#current_password").val() == "") {
            toastr.error("Enter current password", "Error");
            return false;
        } else if ($("#new_password").val() == "") {
            toastr.error("Enter new password", "Error");
            return false;
        } else if ($("#cnrf_password").val() == "") {
            toastr.error("Enter confirm new password", "Error");
            return false;
        }
        password_data = {
            current_password: $("#current_password").val(),
            new_password: $("#new_password").val(),
            confirm_password: $("#cnrf_password").val()
        };
        console.log(password_data);

        var request = $http({
            method: "POST",
            url:
                APP_URL +
                "/api/v1/user/change/password/" +
                $scope.logined_user.id,
            data: password_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function(data) {
                $scope.changed_password = data.data;
                console.log($scope.changed_password);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function() {
                        $window.location.href = Local_URL + "/login";
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function(data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
