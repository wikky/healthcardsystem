app.controller("health_card_listingController", function (
    $scope,
    $http,
    toastr,
    $window
) {
    //?getting list of cards api

    var request = $http({
        method: "GET",
        url: APP_URL + "/api/v1/admin/health/cards",
        headers: {
            "Content-Type": "application/json"
        }
    });
    request
        .success(function (data) {
            $scope.health_card_list = data.data.data;
            console.log($scope.health_card_list);
            if (data.status_code == 1) {
                // toastr.success(data.message, "Success");
            } else {
                toastr.error(data.message, "Error");
                return false;
            }
        })
        .error(function (data, status, headers, config) {
            toastr.error(data.message, "Error");
        });

    //?add new card function

    $scope.add_new_card = function () {
        if ($("#add_card_title").val() == "") {
            toastr.error("Enter Card Title", "Error");
            return false;
        }
        card_data = {
            title: $("#add_card_title").val()
        };
        var request = $http({
            method: "POST",
            url: APP_URL + "/api/v1/health/card",
            data: card_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.new_card_data = data.data;
                console.log($scope.new_card_data);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?get card function

    $scope.get_card = function (card) {
        console.log(card);
        $scope.card_id = card.id;
        $scope.card_detail = card;
    };
    $scope.edit_card = function () {
        if ($("#edit_card_title").val() == "") {
            toastr.error("Enter Card Title", "Error");
            return false;
        }
        card_data = {
            title: $("#edit_card_title").val()
        };
        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/health/card/" + $scope.card_id,
            data: card_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_card_data = data.data;
                console.log($scope.updated_card_data);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };

    //?change card status

    $scope.change_status = function (card) {
        console.log(card);
        card_data = {
            status: card.status
        };
        var request = $http({
            method: "PUT",
            url: APP_URL + "/api/v1/health/card/status/" + card.id,
            data: card_data,
            headers: {
                "Content-Type": "application/json"
            }
        });
        request
            .success(function (data) {
                $scope.updated_status = data.data;
                console.log($scope.updated_status);
                if (data.status_code == 1) {
                    toastr.success(data.message, "Success");
                    setTimeout(function () {
                        $window.location.reload();
                    }, 800);
                } else {
                    toastr.error(data.message, "Error");
                    return false;
                }
            })
            .error(function (data, status, headers, config) {
                toastr.error(data.message, "Error");
            });
    };
});
