<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_type');
            $table->string('patient_name');
            $table->string('card_no');
            $table->string('cr_no');
            $table->string('health_card_scheme');
            $table->string('ward');
            $table->string('address');
            $table->string('mobile');
            $table->string('package_name');
            $table->string('package_code');
            $table->string('approved_amount');
            $table->string('whether_package_changed');
            $table->string('previous_package_detail');
            $table->string('medco_name');
            $table->string('date');
            $table->string('case_no');
            $table->string('admit_date');
            $table->string('discharged_date');
            $table->string('whether_package_extend');
            $table->string('extended_approved_amount');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_details');
    }
}
