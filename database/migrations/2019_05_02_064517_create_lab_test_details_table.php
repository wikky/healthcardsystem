<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabTestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_test_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->string('department');
            $table->string('test');
            $table->string('receipt_no');
            $table->string('amount');
            $table->string('memo_no');
            $table->string('tag');
            $table->string('patient_cr_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_test_details');
    }
}
