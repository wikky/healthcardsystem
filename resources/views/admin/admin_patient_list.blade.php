@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='admin_patient_listController'>
    @section('sidebar')
    @include('admin.includes.sidebar')
    @show
    <div class="main-section">
        @section('header')
        @include('admin.includes.header')
        @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Patients</h3>
                <!-- <button type="button">Add New</button> -->
            </div>
            <div class="content-section">
                <div class="search-container">
                    <select name="" id="filter_type" ng-model='filter_type' ng-change='change_health_card()' style='padding: .6rem 1rem;
    outline: none;'>
                        <option value="">Select Health Card</option>
                        <option value="@{{health_card.identifier}}" ng-repeat='health_card in health_card_list'>
                            @{{health_card.title}}</option>
                    </select>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>CR Number</th>
                                <th>Name</th>
                                <th>Card Number</th>
                                <th>Ward</th>
                                <th>Health Card Scheme</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat='patient in patient_list'>
                                <td style="color:#23b4fb" ng-click='get_patient(patient)'>@{{patient.cr_no}}</td>
                                <td class='text-capitalize'>@{{patient.patient_name}}</td>
                                <td class='text-capitalize'>@{{patient.card_no}}</td>
                                <td class='text-capitalize'>@{{patient.ward}}</td>
                                <td class='text-capitalize'> <span
                                        ng-show='patient.health_card_scheme'>@{{patient.health_card_scheme}}</span>
                                    <span ng-show='!patient.health_card_scheme'>-</span> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/patient_listing.css')}}">
<script src="{{URL::asset('controllers/admin_patient_listController.js')}}"></script>
@endsection