@extends('admin.layout.auth') 
@section('title','') 
@section('content')
<div class="wrapper" ng-controller='lab_test_listingController'>
	@section('sidebar')
	@include('admin.includes.sidebar')
	@show
	<div class="main-section">
		@section('header') 
		@include('admin.includes.header')
		@show
		<div class="content-container">
			<div class="content-heading">
				<h3>Lab Tests</h3>
				<button type="button" data-target='#add_test' data-toggle='modal'>Add New</button>
			</div>
			<div class="content-section">
				<div class="search-container">
					<table class="table table-responsive">
						<thead>
							<tr>
								<th>Test Title</th>
								<th>Department</th>
								<th>Amount</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat='lab_test in lab_test_list'>
								<td style="color:#23b4fb">@{{lab_test.title}}</td>
								<td>@{{lab_test.department_detail[0].title}}</td>
								<td>@{{lab_test.amount}}</td>
								<td>
									<md-switch class="md-primary" ng-change='change_status(lab_test)' md-no-ink aria-label="Switch No Ink" ng-true-value="'1'" ng-false-value="'0'" ng-model="lab_test.status">
											
										</md-switch>
								</td>
								<td><i class="icon-pencil-edit-button" data-target='#edit_test' data-toggle='modal' ng-click='get_test(lab_test)'></i></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	  <!-- edit lab test Modal -->
		<div id="edit_test" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Lab Test</h4>
			</div>
			<div class="modal-body">
				<div class="input_container">
					<div class="input_field">	
						<label for="">Test Title</label>
						<input type="text" placeholder='Test Title' id='edit_test_title' ng-model='test_detail.title'>
					</div>
					<div class="input_field input--field">	
						<label for="">Department</label>
						<select name="" id="edit_department" ng-model='test_detail.department' >
							<option value="">Select Department</option>
							<option value="@{{department.id}}"ng-repeat='department in department_list'>@{{department.title}}</option>
						</select>
					</div>
				</div>
				<div class="input_container">
					<div class="input_field">	
						<label for="">Amount</label>
						<input type="text" placeholder='Amount' id='edit_amount' ng-model='test_detail.amount'>
					</div>
				</div>
                <div class="input_container">
                    <div class="input_field">	
                        <button type='button' ng-click='update_test()'>Update</button>
                    </div>
                </div>
			</div>
		</div>
		</div>
    </div>
    <!-- Add lab test Modal -->
		<div id="add_test" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Lab Test</h4>
			</div>
			<div class="modal-body">
				<div class="input_container">
					<div class="input_field">	
						<label for="">Test Title</label>
						<input type="text" placeholder='Test Title' id='test_title'>
					</div>
					<div class="input_field input--field">	
						<label for="">Department</label>
						<select name="" id="department" >
							<option value="">Select Department</option>
							<option value="@{{department.id}}"ng-repeat='department in department_list'>@{{department.title}}</option>
						</select>
					</div>
				</div>
				<div class="input_container">
					<div class="input_field">	
						<label for="">Amount</label>
						<input type="text" placeholder='Amount' id='amount'>
					</div>
				</div>
                <div class="input_container">
                    <div class="input_field">	
                        <button type='button' ng-click='add_test()'>Submit</button>
                    </div>
                </div>
			</div>
		</div>
		</div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/lab_test_listing.css')}}">
<script src="{{URL::asset('controllers/lab_test_listingController.js')}}"></script>
@endsection