@extends('admin.layout.auth') 
@section('title','') 
@section('content')
<div class="wrapper"  ng-controller='add_patientController'>
   @section('sidebar')
   @include('admin.includes.sidebar')
   @show
    <div class="main-section">
       @section('header')
       @include('admin.includes.header')
       @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Add Patient</h3>
            </div>
            <div class="content-section">
                <div class="input_container">
                    <div class="input_field">
                        <label for="">Patient Name</label>
                        <input type="text" id='patient_name' placeholder='Name'>
                    </div>
                     <div class="input_field">
                        <label for="">Card Number</label>
                        <input type="text" id='card_number' placeholder='Card Number'>
                    </div>
                </div>
                <div class="input_container">
                    <div class="input_field">
                        <label for="">CR Number</label>
                        <input type="text" id='cr_number' placeholder='CR Number'>
                    </div>
                     <div class="input_field">
                        <label for="">Ward Name</label>
                        <input type="text" id='ward_name' placeholder='Ward Name'>
                    </div>
                </div>
                 <div class="input_container">
                    <div class="input_field">
                      <button type='button' ng-click='add_patient()'>Add</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/add_patient.css')}}">
<script src="{{URL::asset('controllers/add_patientController.js')}}"></script>
@endsection
