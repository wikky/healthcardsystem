@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='user_listingController'>
	@section('sidebar')
	@include('admin.includes.sidebar')
	@show
	<div class="main-section">
		@section('header')
		@include('admin.includes.header')
		@show
		<div class="content-container">
			<div class="content-heading">
				<h3>Users</h3>
				<!-- <button type="button">Add New</button> -->
			</div>
			<div class="content-section">
				<div class="search-container">
					<div class="filter_container">
						<input type="text" placeholder='Search by name' ng-model='test'>
						<select name="" id="filter_type" ng-model='filter_type' ng-change='change_user_type()'>
							<option value="">Select Department</option>
							<option value="@{{user_type.identifier}}" ng-repeat='user_type in user_type_list'>
								@{{user_type.title}}</option>
						</select>
					</div>
					<table class="table table-responsive">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>User Type</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat='user in users_list | filter:test' ng-show='user.user_type!="admin"'>
								<td style="color:#23b4fb" data-target='#edit_patient' data-toggle='modal'
									ng-click='get_user(user)'>#@{{user.id}}</td>
								<td class='text-capitalize'>@{{user.first_name}} @{{user.last_name}}</td>
								<td>@{{user.email}}</td>
								<td>@{{user.mobile}}</td>
								<td class='text-capitalize'>@{{user.user_type}}</td>
								<td>
									<md-switch class="md-primary" ng-change='change_status(user)' md-no-ink
										aria-label="Switch No Ink" ng-true-value="'1'" ng-false-value="'0'"
										ng-model="user.status">
									</md-switch>
								</td>
								<td><i class="icon-pencil-edit-button" ng-click='get_user(user)'
										data-target='#edit_patient' data-toggle='modal'></i></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Add Health Card Modal -->
	<div id="edit_patient" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit User</h4>
				</div>
				<div class="modal-body">
					<div class="input_container">
						<div class="input_field">
							<label for="">First Name</label>
							<input type="text" placeholder='First Name' id='fname' ng-model='user_detail.first_name'>
						</div>
						<div class="input_field input--field">
							<label for="">Last Name</label>
							<input type="text" placeholder='Last Name' id='lname' ng-model='user_detail.last_name'>
						</div>
					</div>
					<div class="input_container">
						<div class="input_field">
							<label for="">Mobile</label>
							<input type="text" placeholder='Mobile' id='mobile' ng-model='user_detail.mobile'>
						</div>
						<div class="input_field input--field">
							<label for="">Land Line</label>
							<input type="text" placeholder='Land Line' id='land_line'
								ng-model='user_detail.landline_no'>
						</div>
					</div>
					<div class="input_container" ng-show='user_detail.user_type=="operator"'>
						<div class="input_field">
							<label for="">Health Card Scheme</label>
							<select name="" id="helath_card" ng-model='user_detail.health_card_scheme'>
								<option value="">Select Card </option>
								<option value="@{{health_card.identifier}}" ng-repeat='health_card in health_card_list'>
									@{{health_card.title}}</option>
							</select>
						</div>
					</div>
					<div class="input_container">
						<div class="input_field text_field input--field">
							<label for="">Address</label>
							<textarea name="" id="address" cols="30" rows="2" placeholder='Address'
								ng-model='user_detail.address'></textarea>
						</div>
					</div>
					<div class="input_container">
						<div class="input_field">
							<button type='button' ng-click='update_user()'>Update</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/user_listing.css')}}">
<script src="{{URL::asset('controllers/user_listingController.js')}}"></script>
@endsection