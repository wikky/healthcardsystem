@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='dashboardController'>
    @section('sidebar')
    @include('admin.includes.sidebar')
    @show
    <div class="main-section">
        @section('header')
        @include('admin.includes.header')
        @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Dashboard</h3>
            </div>
            <div class="content-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="content">
                                <h3>Total Users</h3>
                                <p>@{{dashboard_data.total_users}}</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="content">
                                <h3>Total Patients</h3>
                                <p>@{{dashboard_data.total_patients}}</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="content">
                                <h3>Total Health Cards</h3>
                                <p>@{{dashboard_data.total_health_cards}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="{{URl::asset('assets/css/dashboard.css')}}">
    <script src="{{URL::asset('controllers/dashboardController.js')}}"></script>
</div>

@endsection