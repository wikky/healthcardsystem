@extends('admin.layout.auth') 
@section('title','') 
@section('content')
	<div class="wrapper" ng-controller='department_listController'>
		@section('sidebar')
		@include('admin.includes.sidebar')
		@show
		<div class="main-section">
			@section('header')
			@include('admin.includes.header')
			@show
			<div class="content-container">
				<div class="content-heading">
					<h3>Departments</h3>
					<button type="button" data-target='#add_department' data-toggle='modal' >Add New</button>
				</div>
				<div class="content-section">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-3" ng-repeat='department in department_list'>
								<div class="health_container">
									<label for="">Title</label><i class="icon-pencil-edit-button" data-target='#edit_department' data-toggle='modal' ng-click='get_department(department)' ></i>
									<h3>
										<span>@{{department.title}}</span><p>
										<md-switch class="md-primary" ng-change='change_status(department)' md-no-ink aria-label="Switch No Ink" ng-true-value="'1'" ng-false-value="'0'" ng-model="department.status">
											
										</md-switch>
										
									</p>	</h3>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Add Health Card Modal -->
		<div id="add_department" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Department</h4>
			</div>
			<div class="modal-body">
				<div class="input_container">
					<input type="text" placeholder='Department Title' id='add_department_title'>
					<button type='button' ng-click='add_new_department()'>Submit</button>
				</div>
			</div>
			</div>

		</div>
		</div>

		<!-- Edit Health Card Modal -->
		<div id="edit_department" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Department</h4>
			</div>
			<div class="modal-body">
				<div class="input_container">
					<input type="text" placeholder='Department Title' id='edit_department_title' ng-model='department_detail.title' >
					<button type='button' ng-click='edit_department()'>Update</button>
				</div>
			</div>
			</div>

		</div>
		</div>
	</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/department_list.css')}}">
<script src="{{URL::asset('controllers/department_listController.js')}}"></script>
@endsection