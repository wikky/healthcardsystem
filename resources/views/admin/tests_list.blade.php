@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='tests_listController'>
    @section('sidebar')
    @include('admin.includes.sidebar')
    @show
    <div class="main-section">
        @section('header')
        @include('admin.includes.header')
        @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Users</h3>
                <!-- <button type="button">Add New</button> -->
            </div>
            <div class="content-section">
                <div class="search-container">
                    <div class="filter_container">
                        <div class="from_to">
                            <input type="text" id='date_from' placeholder='From Date'>
                            <input type="text" id="date_to" placeholder='To Date'>
                            <button type="button" ng-click='get_list()'>Generate</button>
                        </div>
                        <div class="only_date">
                            <input type="text" id="date" placeholder='Date' ng-change='get_date_data()' ng-model='fff'>
                        </div>
                    </div>
                    <table class="table table-responsive" ng-show='tests_list'>
                        <thead>
                            <tr>
                                <th>Department</th>
                                <th>Test</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat='test in tests_list'>
                                <td class='text-capitalize'>@{{test.department_name}}</td>
                                <td class='text-capitalize'>@{{test.test_name}}</td>
                                <td>@{{test.amount}}</td>
                                <td>@{{test.formated_date}}</td>
                                <td>@{{test.formated_time}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div ng-repeat='date_test in tests_list_date' class="container_div" ng-show='tests_list_date'>
                        <h2>Cr Number : @{{date_test.cr_no}}</h2>

                        <table class="table main-tbl table-responsive">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Test</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat='test in date_test.data' style="border-bottom:1px solid #000">
                                    <td style="padding:0">
                                        <table class="table" style="margin:0">
                                            <tr ng-repeat='test_main in test'>
                                                <td>@{{test_main.department_name}}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding:0">
                                        <table class="table" style="margin:0">
                                            <tr ng-repeat='test_main in test'>
                                                <td class='text-capitalize'>@{{test_main.test_name}}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding:0">
                                        <table class="table" style="margin:0">
                                            <tr ng-repeat='test_main in test'>
                                                <td>@{{test_main.amount}}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding:0">
                                        <table class="table" style="margin:0">
                                            <tr ng-repeat='test_main in test'>
                                                <td>@{{test_main.formated_date}}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding:0">
                                        <table class="table" style="margin:0">
                                            <tr ng-repeat='test_main in test'>
                                                <td>@{{test_main.formated_time}}</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Add Health Card Modal -->
    <div id="edit_patient" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                    <div class="input_container">
                        <div class="input_field">
                            <label for="">First Name</label>
                            <input type="text" placeholder='First Name' id='fname' ng-model='user_detail.first_name'>
                        </div>
                        <div class="input_field input--field">
                            <label for="">Last Name</label>
                            <input type="text" placeholder='Last Name' id='lname' ng-model='user_detail.last_name'>
                        </div>
                    </div>
                    <div class="input_container">
                        <div class="input_field">
                            <label for="">Mobile</label>
                            <input type="text" placeholder='Mobile' id='mobile' ng-model='user_detail.mobile'>
                        </div>
                        <div class="input_field input--field">
                            <label for="">Land Line</label>
                            <input type="text" placeholder='Land Line' id='land_line'
                                ng-model='user_detail.landline_no'>
                        </div>
                    </div>
                    <div class="input_container" ng-show='user_detail.user_type=="operator"'>
                        <div class="input_field">
                            <label for="">Health Card Scheme</label>
                            <select name="" id="helath_card" ng-model='user_detail.health_card_scheme'>
                                <option value="">Select Card </option>
                                <option value="@{{health_card.identifier}}" ng-repeat='health_card in health_card_list'>
                                    @{{health_card.title}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="input_container">
                        <div class="input_field text_field input--field">
                            <label for="">Address</label>
                            <textarea name="" id="address" cols="30" rows="2" placeholder='Address'
                                ng-model='user_detail.address'></textarea>
                        </div>
                    </div>
                    <div class="input_container">
                        <div class="input_field">
                            <button type='button' ng-click='update_user()'>Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/tests_list.css')}}">
<script src="{{URL::asset('controllers/tests_listController.js')}}"></script>
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function(){
    $('#date').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#date_from').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#date_to').datepicker({
        format: 'yyyy-mm-dd'
    });

   
})
</script>
@endsection