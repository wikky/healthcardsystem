@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='registerController'>
    <div class="login_container">
        <div class="logo">
            <h3>Health Card System</h3>
        </div>
        <div class="input_conatiner">
            <div class="input_field input__field">
                <label for="">First Name</label>
                <input type="text" id='fname' placeholder='First Name'>
            </div>
            <div class="input_field input__field2">
                <label for="">Last Name</label>
                <input type="text" id='lname' placeholder='Last Name'>
            </div>
            <div class="input_field">
                <label for="">Email</label>
                <input type="email" id='email' placeholder='Email'>
            </div>
            <div class="input_field input__field">
                <label for="">Password</label>
                <input type="password" id='password' placeholder='Password'>
            </div>
            <div class="input_field input__field2">
                <label for="">User Type</label>
                <select name="" id="user_type" mg-model='user_type'>
                    <option value="">Select User Type</option>
                    <option value="@{{user_type.identifier}}" ng-repeat='user_type in user_type_list'
                        ng-show='user_type.identifier!="admin"'>
                        @{{user_type.title}}</option>
                </select>
            </div>
            <div class="input_field input__field2">
                <label for="">Land Line </label>
                <input type="number" id='land_line' placeholder='Land Line'>
            </div>
            <div class="input_field input__field">
                <label for="">Mobile</label>
                <input type="number" id='mobile' placeholder='Mobile'>
            </div>

            <div class="input_field">
                <label for="">Address</label>
                <textarea name="" id="address" cols="30" rows="2"></textarea>
            </div>
            <div class="input_field">
                <button type='button' ng-click='register()'>Register</button>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/register.css')}}">
<script src="{{URL::asset('controllers/registerController.js')}}"></script>
@endsection