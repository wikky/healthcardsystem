@extends('admin.layout.auth') 
@section('title','') 
@section('content')
<div class="wrapper"  ng-controller='change_passwordController'>
   @section('sidebar')
   @include('admin.includes.sidebar')
   @show
    <div class="main-section">
       @section('header')
       @include('admin.includes.header')
       @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Change Password</h3>
            </div>
            <div class="content-section">
                <div class="input_container">
                    <div class="input_field">
                        <label for="">Current Password</label>
                        <input type="password" id='current_password' placeholder='Current Password'>
                    </div>
                     <div class="input_field">
                        <label for="">New Password</label>
                        <input type="password" id='new_password' placeholder='New Password'>
                    </div>
                     <div class="input_field">
                        <label for="">Confirm New Password</label>
                        <input type="password" id='cnrf_password' placeholder='Confirm New Password'>
                    </div>
                </div>
               
                 <div class="input_container">
                    <div class="input_field">
                      <button type='button' ng-click='change_password()'>Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/change_password.css')}}">
<script src="{{URL::asset('controllers/change_passwordController.js')}}"></script>
@endsection
