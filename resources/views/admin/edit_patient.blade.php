@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='edit_patientController'>
    @section('sidebar')
    @include('admin.includes.sidebar')
    @show
    <div class="main-section">
        @section('header')
        @include('admin.includes.header')
        @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Edit Patient</h3>
            </div>
            <div class="content-section">
                <form>
                    <div class="input_container">
                        <h4 class='section-heading'>Patient Detail</h4>
                        <div class="input_field">
                            <label for="">Patient Name</label>
                            <input type="text" id='patient_name' class="text-capitalize" ng-model='patient.patient_name'
                                placeholder='Name' readonly>
                        </div>
                        <div class="input_field">
                            <label for="">Card Number</label>
                            <input type="text" id='card_number' ng-model='patient.card_no' placeholder='Card Number'
                                readonly>
                        </div>
                        <div class="input_field">
                            <label for="">CR Number</label>
                            <input type="text" id='cr_number' ng-model='patient.cr_no' placeholder='CR Number' readonly>
                        </div>
                        <div class="input_field input--field">
                            <label for="">Ward Name</label>
                            <input type="text" id='ward_name' ng-model='patient.ward' placeholder='Ward Name' readonly>
                        </div>
                        <div class="input_field">
                            <label for="">Health Card Scheme</label>
                            <input type="text" id='health_card' ng-model='logined_user.health_card_scheme'
                                placeholder='Health Card Scheme' class="text-capitalize" readonly>
                        </div>
                        <div class="input_field">
                            <label for="">Mobile</label>
                            <input type="text" id='mobile' placeholder='Mobile' maxlength="10"
                                ng-model='patient.mobile'>
                        </div>
                        <div class="input_field">
                            <label for="">Address</label>
                            <textarea name="" id="address" cols="30" rows="1" placeholder='Address'
                                ng-model='patient.address'></textarea>
                        </div>
                        <div class="input_field input--field">
                            <label for="">Age</label>
                            <input type="text" id='age' placeholder='Age' ng-model='patient.age'>
                        </div>
                        <div class="input_field ">
                            <label for="">Sex</label>
                            <select name="" id="sex" ng-model='patient.sex'>
                                <option value="">Select</option>
                                <option value="m">Male</option>
                                <option value="f">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="input_container">
                        <h4 class='section-heading'>Case Detail</h4>
                        <div class="input_field">
                            <label for="">Case Number</label>
                            <input type="text" id='case_number' placeholder='Case Number' ng-model='patient.case_no'>
                        </div>
                        {{-- <div class="input_field">
                        <label for="">Date</label>
                        <input type="text" id='date' placeholder='Date' ng-model='patient.date'>
                    </div>
                    <div class="input_field">
                        <label for="">Admit Date</label>
                        <input type="text" id='admit_date' placeholder='Admit Date' ng-model='patient.admit_date'>
                    </div>
                    <div class="input_field input--field">
                        <label for="">Discharge Date</label>
                        <input type="text" id='discharge_date' placeholder='Discharge Date'
                            ng-model='patient.discharged_date'>
                    </div> --}}
                        <div class="input_field">
                            <label for="">Package Amount</label>
                            <input type="text" id='approved_amount' placeholder='Package Amount'
                                ng-model='patient.approved_amount'>
                        </div>
                        {{-- <div class="input_field">
                            <label for="">Extended Case Number</label>
                            <input type="text" id='extnd_case_no' placeholder='Extended Case Number'
                                ng-model='patient.extended_case_no'>
                        </div>
                        <div class="input_field input--field">
                            <label for="">Extended Approved Amount</label>
                            <input type="text" id='extend_approved_amount' placeholder='Extended Approved Amount'
                                ng-model='patient.extended_approved_amount'>
                        </div> --}}
                    </div>
                    <div class="input_container">
                        <h4 class='section-heading'>Package Detail</h4>
                        <div class="input_field">
                            <label for="">MEDCO Name</label>
                            <input type="text" id='medco_name' class="text-capitalize" placeholder='MEDCO Name'
                                value='@{{logined_user.first_name}} @{{logined_user.last_name}}' readonly>
                        </div>
                        <div class="input_field">
                            <label for="">Package Code</label>
                            <input type="text" id='package_code' placeholder='Package Code'
                                ng-model='patient.package_code'>
                        </div>
                        <div class="input_field">
                            <label for="">Package Name</label>
                            <input type="text" id='package_name' placeholder='Package Name'
                                ng-model='patient.package_name'>
                        </div>
                        {{-- <div class="input_field input--field">
                            <label for="">Package Changed</label>
                            <select name="" id="package_changed" ng-model='patient.whether_package_changed'>
                                <option value="N">No</option>
                                <option value="Y">Yes</option>
                            </select>
                        </div>
                        <div class="input_field">
                            <label for="">Package Extended</label>
                            <select name="" id="package_extended" ng-model='patient.whether_package_extend'>
                                <option value="N">No</option>
                                <option value="Y">Yes</option>
                            </select>
                        </div>
                        <div class="input_field textarea--field"
                            ng-show='patient.whether_package_changed=="Y" || patient.whether_package_extend=="Y"'>
                            <label for="">Previous Package Detail</label>
                            <textarea name="" id="prev_package_detail" cols="30" rows="1"
                                placeholder='Previous Package Detail'
                                ng-model='patient.previous_package_detail'></textarea>
                        </div> --}}
                    </div>
                    <div class="input_container">
                        <div class="input_field input_field4">
                            <button type='button' ng-click='edit_patient()'>Update</button>
                            <button type='button' ng-click='print_data_basic()'>Print Basic</button>
                            <button type='button' ng-click='print_data()'>Print Detail</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{URL::asset('assets/css/edit_patient.css')}}">
{{-- <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script> --}}
<script src="{{URL::asset('controllers/edit_patientController.js')}}"></script>
{{-- <script>
    $(document).ready(function(){
    $('#date').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#admit_date').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#discharge_date').datepicker({
        format: 'yyyy-mm-dd'
    });

   
})
</script> --}}
@endsection