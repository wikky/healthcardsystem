<!Doctype html>
<html>
   <head>
      <!-- <link href="{{URL::asset('assets/images/fev_icon.png')}}" type="image/x-icon" rel="shortcut icon"> -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title><?php echo env("APP_NAME", "");?> @yield('title')</title>
      <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{URL::asset('assets/fonts/stylesheet.css')}}">
      <link rel="stylesheet" href="{{URL::asset('assets/icons/style.css')}}">
      <link rel="stylesheet" href="{{URL::asset('assets/css/angular-material.min.css')}}">
      <link rel="stylesheet" href="{{URL::asset('assets/css/toastr.css')}}">
      <link rel="stylesheet" href="{{URL::asset('assets/css/main.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
	  
	  <style>
         .md-virtual-repeat-container .md-virtual-repeat-sizer{height:auto!important;}
         toastr.options = {
            toastClass: 'alert',
            iconClasses: {
                  error: 'alert-error',
                  wait: 'alert-info',
                  success: 'alert-success',
                  warning: 'alert-warning'
               },
         } 
      
      </style>
    
      <script> var APP_URL = '<?php echo env("API_URL", "");?>';  </script>
      <script> var Local_URL = '<?php echo env("APP_URL", "");?>';  </script>
      <script src="{{URL::asset('assets/js/jquery-3.3.1.min.js')}}"></script>
      <script src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
      <script src="{{URL::asset('assets/js/angular.js')}}"></script>
      <script src="{{URL::asset('controllers/mainController.js')}}"></script>
      <script src="{{URL::asset('assets/js/angular-animate.js')}}"></script>
      <script src="{{URL::asset('assets/js/angular-aria.js')}}"></script>
      <script src="{{URL::asset('assets/js/angular-messages.js')}}"></script>
      <script src="{{URL::asset('assets/js/angular-material.min.js')}}"></script>
      <script src="{{URL::asset('assets/js/angular-route.min.js')}}"></script>
      <script src="{{URL::asset('assets/js/toastr.js')}}"></script>
      <script src="{{URL::asset('assets/jquery-validator/dist/jquery.validate.min.js')}}"></script>
   </head>
   <body ng-app="myApp" ng-cloak ng-controller='mainController'>
      @yield('content')
   </body>

   <script>
      $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

   </script>
 
  
</html>
