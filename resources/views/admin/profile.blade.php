@extends('admin.layout.auth') 
@section('title','') 
@section('content')
<div class="wrapper"  ng-controller='profileController'>
   @section('sidebar')
   @include('admin.includes.sidebar')
   @show
    <div class="main-section">
       @section('header')
       @include('admin.includes.header')
       @show
        <div class="content-container">
            <div class="content-heading">
                <h3>My Profile</h3>
            </div>
            <div class="content-section">
                <div class="input_container">
                    <div class="input_field">
                        <label for="">First Name</label>
                        <input type="text" id='fname' placeholder='First Name' ng-model='logined_user.first_name' >
                    </div>
                     <div class="input_field">
                        <label for="">Last Name</label>
                        <input type="text" id='lname' placeholder='Last Name'  ng-model='logined_user.last_name'>
                    </div>
                </div>
                <div class="input_container">
                     <div class="input_field input__field">
                        <label for="">Email</label>
                        <input type="text" id='email' placeholder='Email'  ng-model='logined_user.email' readonly>
                    </div>
                     <div class="input_field">
                        <label for="">Mobile</label>
                        <input type="text" id='mobile' placeholder='Mobile'  ng-model='logined_user.mobile'>
                    </div>
                </div>
                 <div class="input_container">
                     <div class="input_field">
                        <label for="">Land Line</label>
                        <input type="text" id='land_line' placeholder='Land Line'  ng-model='logined_user.landline_no' >
                    </div>
                     <div class="input_field input__field">
                        <label for="">User Type</label>
                        <input type="text" id='user_type' placeholder='User Type'  ng-model='logined_user.user_type' readonly>
                    </div>
                </div>
                <div class="input_container">
                     <div class="input_field text_field">
                        <label for="">Address</label>
                       <textarea name="" id="address" cols="30" rows="2" placeholder='Address'  ng-model='logined_user.address'></textarea>
                    </div>
                </div>
                 <div class="input_container">
                    <div class="input_field">
                      <button type='button' ng-click='update_user()'>Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/profile.css')}}">
<script src="{{URL::asset('controllers/profileController.js')}}"></script>
@endsection
