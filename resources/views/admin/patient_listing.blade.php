@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='patient_listingController'>
    @section('sidebar')
    @include('admin.includes.sidebar')
    @show
    <div class="main-section">
        @section('header')
        @include('admin.includes.header')
        @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Patients</h3>
                <!-- <button type="button">Add New</button> -->
            </div>
            <div class="content-section">
                <div class="search-container">
                    <input type="text" placeholder="Enter Cr Number" id="get_cr">
                    <button type="button" class="get_me" ng-click='get_cr_patient()'>GET</button>
                    <table class="table table-responsive" ng-show='cr_num && patient_list'>
                        <thead>
                            <tr>
                                <th>CR Number</th>
                                <th>Name</th>
                                <th>Card Number</th>
                                <th>Ward</th>
                                <th>Health Card Scheme</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat='patient in patient_list'>
                                <td style="color:#23b4fb" ng-click='get_patient(patient)'>@{{patient.cr_no}}</td>
                                <td class='text-capitalize'>@{{patient.patient_name}}</td>
                                <td class='text-capitalize'>@{{patient.card_no}}</td>
                                <td class='text-capitalize'>@{{patient.ward}}</td>
                                <td class='text-capitalize'> <span
                                        ng-show='patient.health_card_scheme'>@{{patient.health_card_scheme}}</span>
                                    <span ng-show='!patient.health_card_scheme'>-</span> </td>
                                <td><i class="icon-pencil-edit-button" ng-click='get_patient(patient.cr_no)'></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/patient_listing.css')}}">
<script src="{{URL::asset('controllers/patient_listingController.js')}}"></script>
@endsection