@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='patient_listController'>
	@section('sidebar')
	@include('admin.includes.sidebar')
	@show
	<div class="main-section">
		@section('header')
		@include('admin.includes.header')
		@show
		<div class="content-container">
			<div class="content-heading">
				<h3>Patients</h3>
				<button type="button" data-target='#add_patient' data-toggle='modal'>Add New</button>
			</div>
			<div class="content-section">
				<div class="search-container">
					<input type="text" placeholder="Enter Cr Number" id="get_cr">
					<button type="button" class="get_me" ng-click='get_cr_patient()'>GET</button>
					<table class="table table-responsive" ng-show='cr_num && patient_list'>
						<thead>
							<tr>
								<th>CR Number</th>
								<th>Name</th>
								<th>Card Number</th>
								<th>Ward</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat='patient in patient_list'>
								<td>@{{patient.cr_no}}</td>
								<td class='text-capitalize'>@{{patient.patient_name}}</td>
								<td class='text-capitalize'>@{{patient.card_no}}</td>
								<td class='text-capitalize'>@{{patient.ward}}</td>
								<td><i class="icon-pencil-edit-button" data-target='#edit_patient' data-toggle='modal'
										ng-click='get_patient(patient)'></i></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- Add Health Card Modal -->
		<div id="edit_patient" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Edit Patient</h4>
					</div>
					<div class="modal-body">
						<div class="input_container">
							<div class="input_field">
								<label for="">CR Number</label>
								<input type="text" placeholder='CR Number' id='edit_cr_number'
									ng-model='patient_detail.cr_no'>
							</div>
							<div class="input_field input--field">
								<label for="">Name</label>
								<input type="text" placeholder='Name' id='edit_patient_name'
									ng-model='patient_detail.patient_name'>
							</div>
						</div>
						<div class="input_container">
							<div class="input_field">
								<label for="">Card Number</label>
								<input type="text" placeholder='Card Number' id='edit_card_number'
									ng-model='patient_detail.card_no'>
							</div>
							<div class="input_field input--field">
								<label for="">Ward Name</label>
								<input type="text" placeholder='Ward Name' id='edit_ward_name'
									ng-model='patient_detail.ward'>
							</div>
						</div>
						<div class="input_container">
							<div class="input_field">
								<button type='button' ng-click='update_patient()'>Update</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Add Health Card Modal -->
		<div id="add_patient" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add Patient</h4>
					</div>
					<div class="modal-body">
						<div class="input_container">
							<div class="input_field">
								<label for="">CR Number</label>
								<input type="text" placeholder='CR Number' id='cr_number'>
							</div>
							<div class="input_field input--field">
								<label for="">Name</label>
								<input type="text" placeholder='Name' id='patient_name'>
							</div>
						</div>
						<div class="input_container">
							<div class="input_field">
								<label for="">Card Number</label>
								<input type="text" placeholder='Card Number' id='card_number'>
							</div>
							<div class="input_field input--field">
								<label for="">Ward Name</label>
								<input type="text" placeholder='Ward Name' id='ward_name'>
							</div>
						</div>
						<div class="input_container">
							<div class="input_field">
								<button type='button' ng-click='add_patient()'>Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
		<link rel="stylesheet" href="{{URL::asset('assets/css/patient_list.css')}}">
		<script src="{{URL::asset('controllers/patient_listController.js')}}"></script>
		@endsection