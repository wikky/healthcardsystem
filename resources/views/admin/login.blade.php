@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='loginController'>
    <div class="login_container">
        <div class="logo">
            <h3>Health Card System</h3>
        </div>
        <div class="input_conatiner">
            <div class="input_field">
                <label for="">Email</label>
                <input type="email" id='email' placeholder='Email'>
            </div>
            <div class="input_field">
                <label for="">Password</label>
                <input type="password" id='password' placeholder='Password'>
            </div>
            {{-- <div class="input_field input__field2">
                <label for="">User Type</label>
                <select name="" id="user_type" ng-model='user_type'>
                    <option value="">Select User Type</option>
                    <option value="@{{user_type.identifier}}" ng-repeat='user_type in user_type_list'>
            @{{user_type.title}}</option>
            </select>
        </div> --}}
        <div class="input_field">
            <button type='button' ng-click='login()'>Login</button>
            <p>Dont Have Account..? <a id='register' href="{{url('register')}}">Register</a></p>
        </div>
    </div>
</div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/login.css')}}">
<script src="{{URL::asset('controllers/loginController.js')}}"></script>
@endsection