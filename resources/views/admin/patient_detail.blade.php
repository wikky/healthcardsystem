@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='patient_detailController'>
    @section('sidebar')
    @include('admin.includes.sidebar')
    @show
    <div class="main-section">
        @section('header')
        @include('admin.includes.header')
        @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Edit Patient</h3>
            </div>
            <div class="content-section">
                <div class="input_container">
                    <h4 class='section-heading'>Patient Detail</h4>
                    <div class="input_field">
                        <label for="">Patient Name</label>
                        <input type="text" id='patient_name' ng-model='patient.patient_name' placeholder='Name'
                            readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Card Number</label>
                        <input type="text" id='card_number' ng-model='patient.card_no' placeholder='Card Number'
                            readonly>
                    </div>
                    <div class="input_field">
                        <label for="">CR Number</label>
                        <input type="text" id='cr_number' ng-model='patient.cr_no' placeholder='CR Number' readonly>
                    </div>
                    <div class="input_field input--field">
                        <label for="">Ward Name</label>
                        <input type="text" id='ward_name' ng-model='patient.ward' placeholder='Ward Name' readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Health Card</label>
                        <select name="" id="helath_card" ng-model='patient.health_card_scheme' disabled>
                            <option value="">Select Card </option>
                            <option value="@{{health_card.identifier}}" ng-repeat='health_card in health_card_list'>
                                @{{health_card.title}}</option>
                        </select>
                    </div>
                    <div class="input_field">
                        <label for="">Mobile</label>
                        <input type="text" id='mobile' placeholder='Mobile' ng-model='patient.mobile' readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Address</label>
                        <textarea name="" id="address" cols="30" rows="1" placeholder='Address'
                            ng-model='patient.address' readonly></textarea>
                    </div>
                    <div class="input_field input--field">
                        <label for="">Sex</label>
                        <select name="" id="sex" ng-model='patient.sex' disabled>
                            <option value="">Select</option>
                            <option value="m">Male</option>
                            <option value="f">Female</option>
                        </select>
                    </div>
                </div>
                <div class="input_container">
                    <h4 class='section-heading'>Case Detail</h4>
                    <div class="input_field">
                        <label for="">Case Number</label>
                        <input type="text" id='case_number' placeholder='Case Number' ng-model='patient.case_no'
                            readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Date</label>
                        <input type="text" id='date' placeholder='Date' ng-model='patient.date' readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Admit Date</label>
                        <input type="text" id='admit_date' placeholder='Admit Date' ng-model='patient.admit_date'
                            readonly>
                    </div>
                    <div class="input_field input--field">
                        <label for="">Discharge Date</label>
                        <input type="text" id='discharge_date' placeholder='Discharge Date'
                            ng-model='patient.discharged_date' readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Approved Amount</label>
                        <input type="text" id='approved_amount' placeholder='Approved Amount'
                            ng-model='patient.approved_amount' readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Extended Case Number</label>
                        <input type="text" id='extnd_case_no' placeholder='Extended Case Number'
                            ng-model='patient.extended_case_no'>
                    </div>
                    <div class="input_field">
                        <label for="">Extended Approved Amount</label>
                        <input type="text" id='extend_approved_amount' placeholder='Extended Approved Amount'
                            ng-model='patient.extended_approved_amount' readonly>
                    </div>
                </div>
                <div class="input_container">
                    <h4 class='section-heading'>Package Detail</h4>
                    <div class="input_field">
                        <label for="">MEDCO Name</label>
                        <input type="text" id='medco_name' placeholder='MEDCO Name' ng-model='patient.medco_name'
                            readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Package Code</label>
                        <input type="text" id='package_code' placeholder='Package Code' ng-model='patient.package_code'
                            readonly>
                    </div>
                    <div class="input_field">
                        <label for="">Package Name</label>
                        <input type="text" id='package_name' placeholder='Package Name' ng-model='patient.package_name'
                            readonly>
                    </div>
                    <div class="input_field input--field">
                        <label for="">Package Changed</label>
                        <select name="" id="package_changed" ng-model='patient.whether_package_changed' disabled>
                            <option value="N">No</option>
                            <option value="Y">Yes</option>
                        </select>
                    </div>
                    <div class="input_field">
                        <label for="">Package Extended</label>
                        <select name="" id="package_extended" ng-model='patient.whether_package_extend' disabled>
                            <option value="N">No</option>
                            <option value="Y">Yes</option>
                        </select>
                    </div>
                    <div class="input_field textarea--field">
                        <label for="">Previous Package Detail</label>
                        <textarea name="" id="prev_package_detail" cols="30" rows="1"
                            placeholder='Previous Package Detail' ng-model='patient.previous_package_detail'
                            readonly></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/patient_detail.css')}}">

<script src="{{URL::asset('controllers/patient_detailController.js')}}"></script>

@endsection