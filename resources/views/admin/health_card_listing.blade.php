@extends('admin.layout.auth') 
@section('title','') 
@section('content')
	<div class="wrapper" ng-controller='health_card_listingController'>
		@section('sidebar')
		@include('admin.includes.sidebar')
		@show
		<div class="main-section">
			@section('header')
			@include('admin.includes.header')
			@show
			<div class="content-container">
				<div class="content-heading">
					<h3>Health Cards</h3>
					<button type="button" data-target='#add_healthCard' data-toggle='modal' >Add New</button>
				</div>
				<div class="content-section">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-3" ng-repeat='health_card in health_card_list'>
								<div class="health_container">
									<label for="">Title</label><i class="icon-pencil-edit-button" data-target='#edit_healthCard' data-toggle='modal' ng-click='get_card(health_card)' ></i>
									<h3>
										<span>@{{health_card.title}}</span><p>
										<md-switch class="md-primary" ng-change='change_status(health_card)' md-no-ink aria-label="Switch No Ink" ng-true-value="'1'" ng-false-value="'0'" ng-model="health_card.status">
											
										</md-switch>
										
									</p>	</h3>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Add Health Card Modal -->
		<div id="add_healthCard" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Card</h4>
			</div>
			<div class="modal-body">
				<div class="input_container">
					<input type="text" placeholder='Card Title'id='add_card_title'>
					<button type='button' ng-click='add_new_card()'>Submit</button>
				</div>
			</div>
			</div>

		</div>
		</div>

		<!-- Edit Health Card Modal -->
		<div id="edit_healthCard" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Card</h4>
			</div>
			<div class="modal-body">
				<div class="input_container">
					<input type="text" placeholder='Card Title' id='edit_card_title' ng-model='card_detail.title' >
					<button type='button' ng-click='edit_card()'>Update</button>
				</div>
			</div>
			</div>

		</div>
		</div>
	</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/health_card_listing.css')}}">
<script src="{{URL::asset('controllers/health_card_listingController.js')}}"></script>
@endsection