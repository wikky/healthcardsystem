<div class="sidebar">
    <div class="logo-holder">
        <h3 class='health'>Health Card System</h3>
    </div>
    <ul>
        <li ng-show='logined_user.user_type=="admin"'
            class="<?php if(request()->segment(count(request()->segments())) == 'dashboard') { echo 'active';} else { echo '';}?>">
            <a href="{{url('dashboard')}}" class="patients">Dashboard</a>
        </li>
        <li ng-show='logined_user.user_type =="admin"'
            class="<?php if(request()->segment(count(request()->segments())) == 'users_listing') { echo 'active';} else { echo '';}?>">
            <a href="{{url('users_listing')}}" class="patients">Users</a>
        </li>
        <li ng-show='logined_user.user_type=="admin"'
            class="<?php if(request()->segment(count(request()->segments())) == 'department_list') { echo 'active';} else { echo '';}?>">
            <a href="{{url('department_list')}}" class="patients">Departments</a>
        </li>
        <li ng-show='logined_user.user_type=="admin"'
            class="<?php if(request()->segment(count(request()->segments())) == 'user_type_listing') { echo 'active';} else { echo '';}?>">
            <a href="{{url('user_type_listing')}}" class="patients">User Types</a>
        </li>
        <li ng-show='logined_user.user_type=="doctor"'
            class="<?php if(request()->segment(count(request()->segments())) == 'patient_list') { echo 'active';} else { echo '';}?>">
            <a href="{{url('patient_list')}}" class="patients">Patients</a>
        </li>
        <li ng-show='logined_user.user_type=="admin"'
            class="<?php if(request()->segment(count(request()->segments())) == 'admin_patient_list') { echo 'active';} else { echo '';}?>">
            <a href="{{url('admin_patient_list')}}" class="patients">Patients</a>
        </li>
        <li ng-show='logined_user.user_type=="operator"'
            class="<?php if(request()->segment(count(request()->segments())) == 'patient_listing') { echo 'active';} else { echo '';}?>">
            <a href="{{url('patient_listing')}}" class="patients">Patients</a>
        </li>
        <li ng-show='logined_user.user_type=="admin"'
            class="<?php if(request()->segment(count(request()->segments())) == 'health_card_listing') { echo 'active';} else { echo '';}?>">
            <a href="{{url('health_card_listing')}}" class="patients">Health Card</a>
        </li>
        <li ng-show='logined_user.user_type=="admin"'
            class="<?php if(request()->segment(count(request()->segments())) == 'lab_test_listing') { echo 'active';} else { echo '';}?>">
            <a href="{{url('lab_test_listing')}}" class="lab">Lab Tests</a>
        </li>
        <li ng-show='logined_user.user_type=="lab_assistant"'
            class="<?php if(request()->segment(count(request()->segments())) == 'bill') { echo 'active';} else { echo '';}?>">
            <a href="{{url('bill')}}" class="lab">Bill</a>
        </li>
        <li ng-show='logined_user.user_type=="lab_assistant"'
            class="<?php if(request()->segment(count(request()->segments())) == 'investigation') { echo 'active';} else { echo '';}?>">
            <a href="{{url('investigation')}}" class="lab">Investigation</a>
        </li>
        <li ng-show='logined_user.user_type=="lab_assistant"'
            class="<?php if(request()->segment(count(request()->segments())) == 'tests_list') { echo 'active';} else { echo '';}?>">
            <a href="{{url('tests_list')}}" class="lab">Tests List</a>
        </li>
    </ul>
</div>