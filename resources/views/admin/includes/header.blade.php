<header>
    <div class="profile-container">
        <div class="profile-dropdown-container">
            <div class="profile_photo text-capitalize" >
                @{{logined_user.first_name|limitTo:1}}
            </div>
            <span class="admin_name text-capitalize"> @{{logined_user.first_name}}</span>
        </div>
    </div>
     <ul class='proff'>
        <li> <a href="{{URL::to('profile')}}">My Profile</a> </li>
        <li> <a href="{{URL::to('change_password')}}">Change Password</a> </li>
        <li><a href="#"class='logout' ng-click='logout()' >Logout</a></li>
    </ul>
</header>
<script>
 
$(document).ready(function(){
        // Show hide popover
        $(".profile-dropdown-container").click(function(){
            $(".proff").slideToggle("fast");
        });
    });
    $(document).on("click", function(event){
        var $trigger = $(".profile-dropdown-container");
        if($trigger !== event.target && !$trigger.has(event.target).length){
            $(".proff").slideUp("fast");
        }            
    }); 
</script>