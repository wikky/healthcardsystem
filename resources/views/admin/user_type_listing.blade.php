@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='user_typeController'>
	@section('sidebar')
	@include('admin.includes.sidebar')
	@show
	<div class="main-section">
		@section('header')
		@include('admin.includes.header')
		@show
		<div class="content-container">
			<div class="content-heading">
				<h3>User Types</h3>
				<button type="button" data-target='#add_user_type' data-toggle='modal'>Add New</button>
			</div>
			<div class="content-section">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-3" ng-repeat='user_type in user_type_list'
							ng-show='user_type.identifier!="admin"'>
							<div class="health_container">
								<label for="">Title</label><i class="icon-pencil-edit-button"
									data-target='#edit_user_type' data-toggle='modal'
									ng-click='get_user_type(user_type)'></i>
								<h3>
									<span>@{{user_type.title}}</span>
									<p>
										<md-switch class="md-primary" ng-change='change_status(user_type)' md-no-ink
											aria-label="Switch No Ink" ng-true-value="'1'" ng-false-value="'0'"
											ng-model="user_type.status">

										</md-switch>

									</p>
								</h3>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Health Card Modal -->
	<div id="add_user_type" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add User Type</h4>
				</div>
				<div class="modal-body">
					<div class="input_container">
						<input type="text" placeholder='User Type Title' id='add_user_type_title'>
						<button type='button' ng-click='add_new_user_type()'>Submit</button>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- Edit Health Card Modal -->
	<div id="edit_user_type" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit User Type</h4>
				</div>
				<div class="modal-body">
					<div class="input_container">
						<input type="text" placeholder='User Type Title' id='edit_user_type_title'
							ng-model='user_type_detail.title'>
						<button type='button' ng-click='edit_user_type()'>Update</button>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/user_type.css')}}">
<script src="{{URL::asset('controllers/user_typeController.js')}}"></script>
@endsection