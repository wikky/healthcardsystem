@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='billController'>
	@section('sidebar')
	@include('admin.includes.sidebar')
	@show
	<div class="main-section">
		@section('header')
		@include('admin.includes.header')
		@show
		<div class="content-container">
			<div class="content-heading">
				<h3>Lab Tests</h3>
				<!-- <button type="button">Add New</button> -->
			</div>
			<div class="content-section">
				<div class="search-container">
					<div class="top_container">
						<input type="text" id='get_user' ng-model='get_user' ng-change='get_user_detail()'
							placeholder="Enter Cr Number">
						<select ng-show='cr_no' name="" id="get_department" ng-model='dapartment_value'
							ng-change='change_department()'>
							<option value="">Select Department</option>
							<option value="@{{department.title}}" data-id='@{{department.id}}'
								ng-repeat='department in department_list'>
								@{{department.title}}</option>
						</select>
						<select ng-show='cr_no' name="" id="get_test" ng-model='test_value' ng-change='change_test()'>
							<option value="">Select Test</option>
							<option value="@{{test.title}}" data-id='@{{test.id}}' ng-repeat='test in lab_test_list'>
								@{{test.title}}</option>
						</select>
						<input type="text" id='amount' value=' @{{amount}}' style='display:none'>
						<i class="icon-plus" ng-click='add_row()' ng-show='cr_no'></i>
					</div>
					<h3 class="title" ng-show='patient_list'>Patient Detail</h3>
					<table class="table table-responsive" ng-show='patient_list'>
						<thead>
							<tr>
								<th>Name</th>
								<th>Sex</th>
								<th>Bill Number</th>
								<th>Req. Number</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat='patient in patient_list'>
								<td class="text-capitalize">
									@{{patient.patient_name}}
								</td>
								<td>
									<span ng-show='patient.sex=="m"'>Male</span>
									<span ng-show='patient.sex=="f"'>Female</span>
								</td>
								<td>
									@{{last_bill}}
								</td>
								<td>
									@{{last_requsition}}
								</td>
								<td>
									@{{current_date}}
								</td>
							</tr>
						</tbody>
					</table>
					<h3 class="title" ng-show='test_Array.length>0 && cr_no'>Test Detail</h3>
					<div id="content_div">
						<table id='print_table' class='table' ng-show='test_Array.length>0 && cr_no' style="width:100%">
							<thead>
								<tr>
									<th>Department</th>
									<th>Test</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="total_container" ng-show='test_Array.length>0 && cr_no'>

						<div class="total" ng-mode> Total : @{{total_amount}}</div>
						<div class="button_container">
							<button type="button" class="saav_me" ng-click='save_data()'>Save</button>
							<button type="button" class="print_me" ng-click='print_bill()'>Print</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/bill.css')}}">
<script src="{{URL::asset('controllers/billController.js')}}"></script>

@endsection