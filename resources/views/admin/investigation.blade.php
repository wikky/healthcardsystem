@extends('admin.layout.auth')
@section('title','')
@section('content')
<div class="wrapper" ng-controller='investigationController'>
    @section('sidebar')
    @include('admin.includes.sidebar')
    @show
    <div class="main-section">
        @section('header')
        @include('admin.includes.header')
        @show
        <div class="content-container">
            <div class="content-heading">
                <h3>Investigation</h3>
                <!-- <button type="button">Add New</button> -->
            </div>
            <div class="content-section">
                <div class="search-container">
                    <div class="top_container">
                        <input type="text" id='get_user' placeholder="Enter requsition Number">
                        <button type="button" class="get" ng-click='get_user_detail()'>GET</button>
                    </div>
                    <h3 class="title" ng-show='patient_detail'>Patient Details</h3>
                    <table class="table table-responsive" ng-show='patient_detail && req_detail_list'>
                        <thead>
                            <tr>
                                <th>CR No.</th>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Sex</th>
                                <th>Mobile</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat='patient in patient_detail'>
                                <td>
                                    @{{patient.cr_no}}
                                </td>
                                <td>
                                    @{{patient.patient_name}}
                                </td>
                                <td>
                                    @{{patient.age}}
                                </td>
                                <td>
                                    <span ng-show='patient.sex=="m"'>Male</span>
                                    <span ng-show='patient.sex=="f"'>Female</span>
                                </td>
                                <td>
                                    @{{patient.mobile}}
                                </td>
                                <td>
                                    @{{patient.address}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h3 class="title" ng-show='req_detail_list'>Test Details</h3>
                    <div id="content_div">
                        <table class="table table-responsive" ng-show='req_detail_list'>
                            <thead>
                                <tr>
                                    <th>Test</th>
                                    <th>Department</th>
                                    <th>Amount</th>
                                    <th>Bill Number</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat='req in req_detail_list'>
                                    <td>
                                        @{{req.test}}
                                    </td>
                                    <td>
                                        @{{req.department}}
                                    </td>
                                    <td>
                                        @{{req.amount}}
                                    </td>
                                    <td>
                                        @{{req.memo_no}}
                                    </td>
                                    <td>
                                        @{{req.date}}
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="total_container" ng-show='req_detail_list'>
                        <div class="button_container">
                            <button type="button" ng-click='print_investigation()'>Print</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{URL::asset('assets/css/investigation.css')}}">
<script src="{{URL::asset('controllers/investigationController.js')}}"></script>

@endsection